﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ManageMikrotikApp.Web {
    public class Program {
        public static void Main(string[] args) {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) {
            string defaultConfigFile = "appsettings.json";
#if DEBUG
            var directory = Directory.GetCurrentDirectory();
            var hostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(directory)
                .ConfigureAppConfiguration((context, configBuilder) => {
                    configBuilder.AddJsonFile(defaultConfigFile, optional: true, reloadOnChange: true);
                    configBuilder.AddEnvironmentVariables();
                })
                .UseStartup<Startup>();
#else
            string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            //once you have the path you get the directory with:
            var directory = System.IO.Path.GetDirectoryName(path).Replace("file:", string.Empty);
            if (args.Length > 0) {
                if (File.Exists(args[0])) {
                    directory = Path.GetDirectoryName(args[0]);
                    defaultConfigFile = Path.GetFileName(args[0]);
                }
                else {
                    new Exception($"file {args[0]} not found");
                }
            }

            if (directory.Substring(0, 1) == @"\") {
                directory = directory.Substring(1);
            }

            var configFilePath = Path.Combine(directory, defaultConfigFile);

            Console.WriteLine("ConfigFilePath: {0}", configFilePath);

            path = System.IO.Path.GetDirectoryName(path).Replace("file:", string.Empty);

            if (path.Substring(0, 1) == @"\") {
                path = path.Substring(1);
            }

            var hostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(path)
                .ConfigureAppConfiguration((context, configBuilder) => {
                    configBuilder.AddJsonFile(defaultConfigFile, optional: true, reloadOnChange: true);
                    configBuilder.AddEnvironmentVariables();
                })
                .UseStartup<Startup>();
#endif

            return hostBuilder;
        }
    }
}
