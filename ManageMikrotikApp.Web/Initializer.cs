﻿using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Concrete;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.PostgreSQL.UnitOfWork;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.PresenationLayer.Facades.Concrete;
using ManageMikrotikApp.PresenationLayer.Services.Abstract;
using ManageMikrotikApp.PresenationLayer.Services.Concrete;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbModels = ManageMikrotikApp.DAL.Abstrasctions.Models;

namespace ManageMikrotikApp.Web {
    internal static class Initializer {
        internal static void InitializeServices(IServiceCollection services) {
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddSingleton<IServiceManagerFactory, ServiceManagerPresentationFactory>();
            services.AddSingleton(serviceProvider => {
                return serviceProvider.GetService<IServiceManagerFactory>().Create();
                });
            services.AddScoped<IUserFacade, UserFacade>();
            services.AddScoped<ISshConnectionFacade, SshConnectionFacade>();
            services.AddScoped<IScriptFacade, ScriptFacade>();
        }

        internal static void InitializeAutoMapper() {
            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<SshConnectionInfo, SshConnectionInfo>();
                cfg.CreateMap<User, User>();
                cfg.CreateMap<DbModels.Script, DbModels.Script>();
                cfg.CreateMap<DbModels.Router, DbModels.Router>();
                cfg.CreateMap<DbModels.CommandToExecute, DbModels.CommandToExecute>();
                cfg.CreateMap<DbModels.ExecuteScriptUsers, DbModels.ExecuteScriptUsers>();
                cfg.CreateMap<DbModels.Settings, DbModels.Settings>();
            });
        }
    }
}
