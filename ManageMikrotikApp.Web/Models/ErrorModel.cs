﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Models {
    public class ErrorModel {
        public bool error { get; set; }
        public string message { get; set; }
    }
}
