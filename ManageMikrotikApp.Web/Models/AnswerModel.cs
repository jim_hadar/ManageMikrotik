﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Models {
    public class AnswerModel<T> {
        public AnswerModel() {
            Success = false;
        }
        public T Obj { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
