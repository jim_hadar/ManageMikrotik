import { axiosInstance, setAuthHeader } from '../../utils/http';
export default {
    created: function () {
        let id = this.$route.params.id;
        if (id) {
            axiosInstance.get(`/api/User/${id}`)
                .then((response) => {                        
                    let data = response.data;
                    if (data.Success) {
                        this.user = data.Obj;
                    }
                    else {
                        this.error = data.Message;
                    }
                });
        }
        else {
            axiosInstance.get(`/api/Standard/GetNewViewModel/UserViewModel`)
                .then((response) => {                        
                    let data = response.data;
                    this.user = data;
                });
        }
    },        
    data: function(){
        return {
            user: null,
            error: null,
            success: null,
            isEditPassword: null,
        };
    },
    computed: {
        isCreateNewUser: function () {
            return this.user && this.user.Id <= 0;
        }
    },
    methods: {
        save: function () {
            let self = this;
            this.error = null;
            this.success = null;            
            if(this.isCreateNewUser){
                axiosInstance.put('/api/User', this.user)
                .then(response => {
                    let data = response.data;                        
                    if(!data.Success){
                        self.error = data.Message;
                    }
                    else{
                        this.user = data.Obj;
                        self.$router.push({ name: 'profile', params: { id: data.Obj.Id } });
                        this.success = "Пользователь успешно сохранен";
                    }
                })
                .catch(error => {
                    self.error = "При сохранении пользователя произошла ошибка";
                });
            }
            else{
                axiosInstance.post(`/api/User/`, this.user)
                    .then(response => {
                        let data = response.data;
                        if (!data.Success) {
                            self.error = data.Message;
                            return;
                        }
                        self.success = "Данные успешно сохранены";
                        this.isEditPassword = false;
                    })
                    .catch((error) => {
                        self.error = "При сохранении пользователя произошла ошибка";
                    });
            }
        },
        editPassword: function(){
            this.isEditPassword = true;
        }
    }
}