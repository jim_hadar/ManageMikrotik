import { Vuetable } from 'vuetable-2';
import { axiosInstance } from '../../../../../utils/http';
import EditRouter from '../Edit';

export default{
    props: ['script'],
    components: {
        'grid': Vuetable,  
        'edit-router': EditRouter  
    },
    data: function(){
        return {
            fields: [
                { name: 'Name', title: 'Имя роутера'},
                { name: 'CommandToExecute', callback: 'getCommands', title: 'Список команд для выполнения' },
                { name: 'SshConnection', callback: 'getFullHostName', title: 'Соединение по ssh' },
                '__slot:actions'
            ],
            css: {
                table: {
                    tableClass: 'table table-striped table-bordered table-hovered',
                    loadingClass: 'loading',
                    ascendingIcon: 'glyphicon glyphicon-chevron-up',
                    descendingIcon: 'glyphicon glyphicon-chevron-down',
                    handleIcon: 'glyphicon glyphicon-menu-hamburger',
                },
            },
        }
    },
    created: function(){

    },
    methods: {
        getCommands: function(value){
            let resString = '';
            if(value){
                for(let command of value){
                    resString += command.Value + '<br/>';
                }                    
            }
            return resString;
        },
        getFullHostName: function(sshConnection){
            return sshConnection ? `${sshConnection.HostName}:${sshConnection.Port}` : '';
        },
        addRouter: function() {
            let self = this;
            axiosInstance.get(`/api/Standard/GetNewViewModel/Router`)
                .then((response) => {                        
                    let data = response.data;
                    self.script.Router.push(data);
                    self.editRow(data);
                });
        },
        editRow: function(rowData){
            this.$refs.editRouter.open(rowData);
        },
        deleteRow: function(rowData){
            let index = this.script.Router.indexOf(rowData);
            let isDelete = confirm('Вы уверены, что хотите удалить роутер?');
            if(isDelete)
                this.script.Router.splice(index, 1);
        },
        addRouterSave: function(){
            this.$refs.vuetable.setData(this.script.Router);
        }
    }
}