import { axiosInstance } from '../../../../../utils/http';
export default{
    data: function(){
        return {
            router: null,
            sshConnections: null,
            error: null
        }
    },
    methods: {
        isActiveConn: function(ssh){
            return this.router && ssh == this.router.SshConnection ? 'alert-success' : '';
        },
        save: function(){
            if(this.validateRouter()){
                $('#routerEditDialog').modal('hide');
                this.$emit('add-router-save');
            }
        },
        open: function(r){
            $('#routerEditDialog').modal('show');
            this.getSshConnections();
            if(!r.CommandToExecute){
                r.CommandToExecute = new Array();
            }
            this.router = r;
        },
        close: function() {
            this.save(); 
        },
        getSshConnections: function () {
            if(this.sshConnections){ return; }
            let self = this;
            axiosInstance.get('/api/SshConnection/?page=1&per_page=1000')
                .then((response) => {
                    if (response.data.Success) {
                        let connections = response.data.Obj.data;
                        connections.forEach((conn) => {
                            conn.isChecked = self.router.SshConnection && conn.Id == self.router.SshConnection;
                        });
                        self.sshConnections = connections;
                    }
                });
        },
        checkSshConnection: function(ssh){
            this.router.SshConnection = ssh;
        },
        editSshConnection: function(){
            this.router.SshConnection = null;                
        },
        addCommandToExecute: function(){
            let self = this;
            axiosInstance.get(`/api/Standard/GetNewViewModel/CommandToExecute`)
                .then((response) => {                        
                    let data = response.data;
                    self.router.CommandToExecute.push(data);
                });
        },
        validateRouter: function(){
            this.error = '';
            let router = this.router;
            if(!router.Name || router.Name && router.Name.trim() == ''){
                this.error = 'Имя роутера не может пустым<br />';
            }
            if(!router.SshConnection){
                this.error += 'Параметры ssh соединения не должны быть пустыми <br/>';
            }
            if(router.CommandToExecute){
                for(let command of router.CommandToExecute){
                    if(!command.Value || command.Name && command.Name.trim() == ''){
                        this.error += 'Команды не должны быть пустыми';
                        break;
                    }
                }
            }
            if(this.error.trim() == ''){
                this.error = null
            }
            return this.error == null;
        },
        deleteCommand: function(command){
            let index = this.router.CommandToExecute.indexOf(command);
            this.router.CommandToExecute.splice(index, 1); 
            for( var i = 0; i < this.router.CommandToExecute.length - 1; i++){ 
                if (this.router.CommandToExecute[i] == command ) {
                    this.router.CommandToExecute.splice(i, 1); 
                    break;
                }
            }
        }
    }
}