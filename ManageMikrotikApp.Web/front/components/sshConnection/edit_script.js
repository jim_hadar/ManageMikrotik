import { axiosInstance, setAuthHeader } from '../../utils/http';
export default {
    created: function(){
        let id = this.$route.params.id;
        if (id) {
            axiosInstance.get(`/api/SshConnection/${id}`)
                .then((response) => {                        
                    let data = response.data;
                    if (data.Success) {
                        this.connection = data.Obj;
                    }
                    else {
                        this.error = data.Message;
                    }
                });
        }
        else {
            axiosInstance.get(`/api/Standard/GetNewViewModel/SshConnection`)
                .then((response) => {                        
                    let data = response.data;
                    this.connection = data;
                });
        }
    },
    data: function(){
        return {
            connection: null,
            error: null,
            success: null
        }
    },
    computed: {
        isNewConnection: function(){
            return this.connection && this.connection.Id <= 0;
        }
    },
    methods: {
        save: function () {
            let self = this;
            this.error = null;
            this.success = null;            
            if(this.isNewConnection){
                axiosInstance.put('/api/SshConnection', this.connection)
                .then(response => {
                    let data = response.data;
                    if(!data.Success){
                        self.error = data.Message;
                    }
                    else{
                        this.connection = data.Obj;
                        self.$router.push({ name: 'editSsh', params: { id: data.Obj.Id } });
                        this.success = "Параметры соединения успешно сохранены";
                    }
                })
                .catch(error => {
                    self.error = "При сохранении параметров соединения произошла ошибка";
                });
            }
            else{
                axiosInstance.post(`/api/SshConnection/`, this.connection)
                    .then(response => {
                        let data = response.data;
                        if (!data.Success) {
                            self.error = data.Message;
                            return;
                        }
                        self.success = "Данные успешно сохранены";
                    })
                    .catch((error) => {
                        self.error = "При сохранении пользователя произошла ошибка";
                    });
            }
        },
    }
}