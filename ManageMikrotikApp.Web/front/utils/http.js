﻿import axios from 'axios';
import store from '../store';

var axiosInstance = axios.create();

function setAuthHeader(){
    if (store.getters.isAuthenticated) {
        axiosInstance.defaults.headers['Authorization'] = `Bearer ${store.state.auth.token}`;
    }    
}

setAuthHeader();

export {
    axiosInstance,
    setAuthHeader
}