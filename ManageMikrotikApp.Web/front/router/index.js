﻿import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../components/login';
import Users from '../components/users/UserList';
import Profile from '../components/users/Profile';
import store from '../store';
import SshList from '../components/sshConnection/SshList';
import SshEdit from '../components/sshConnection/Edit';
import ScriptList from '../components/script/ScriptList';
import EditScript from '../components/script/edit/Edit';
import Settings from '../components/settings';

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/');
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/login');
};

export default new VueRouter({
    //mode: 'history',
    routes: [
        {
            path: '/',
            //name: ''
            component: Users,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/users',
            //name: ''
            component: Users,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/profile/:id',
            name: 'profile',
            component: Profile,
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/create/profile',
            name: 'createProfile',
            component: Profile,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/sshList',
            name: 'sshList',
            component: SshList,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/create/ssh',
            name: 'createSsh',
            component: SshEdit,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/ssh/:id',
            name: 'editSsh',
            component: SshEdit,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/scriptList',
            name: 'scriptList',
            component: ScriptList,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/script/:id',
            name: 'editScript',
            component: EditScript,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/create/script',
            name: 'createScript',
            component: EditScript,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings,
            beforeEnter: ifAuthenticated
        }
    ]
});