﻿import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from '../actions/auth';
import { USER_REQUEST } from '../actions/user';
import axios from 'axios';

const state = { token: localStorage.getItem('user-token') || '', status: '', hasLoadedOnce: false };

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
};

const actions = {
    [AUTH_REQUEST]: ({ commit, dispatch }, user) => {      
        commit(AUTH_REQUEST);
        return axios.post('/token', user)
            .then(function (response) {
                let data = response.data;
                if(!data.Success){
                    throw data;
                }
                localStorage.setItem('user-token', data.Obj.Token);
                commit(AUTH_SUCCESS, response);
            })
            .catch(function (error) {                
                commit(AUTH_ERROR, error);
                localStorage.removeItem('user-token');
                throw error;
            });
    },
    [AUTH_LOGOUT]: ({ commit, dispatch }) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_LOGOUT);
            localStorage.removeItem('user-token');
            resolve();
        });
    }
};

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading';
    },
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = 'success';
        state.hasLoadedOnce = true;
        state.token = localStorage.getItem('user-token');
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error';
        state.hasLoadedOnce = true;
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = '';
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}