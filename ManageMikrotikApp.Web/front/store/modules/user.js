﻿import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from './../actions/user';
import Vue from 'vue';
import { AUTH_LOGOUT } from '../actions/auth';

const state = { status: '', profile: {} };

const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile
};

const actions = {
    [USER_REQUEST]: ({ commit, dispatch }) => {

    }
};

const mutations = {

};