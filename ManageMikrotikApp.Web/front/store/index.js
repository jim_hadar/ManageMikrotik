﻿import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import auth from './modules/auth';

Vue.use(Vuex);
//Vuex.prototype.$http = axios;

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        auth
    },
    strict: debug
});