﻿import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import store from './store';
import router from './router';


//Vue.prototype.$http = axios;

new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App)
});