﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Helpers {
    public class JsonNetResult : JsonResult {
        #region constants
        public const int MAX_DEPTH = 1000;
        public const string DATE_TIME_FORMATE = "dd.MM.yyyy HH:mm:ss";
        public const string DATE_FORMATE = "dd.MM.yyyy";
        public const string MONTH_FORMATE = "MMMM yyyy";
        #endregion

        private object _jObject;
        public JsonNetResult(object value, int? max_depth = null)
            : base(value) {
            SerializerSettings = new JsonSerializerSettings {
                DateFormatString = DATE_TIME_FORMATE,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                StringEscapeHandling = StringEscapeHandling.EscapeHtml,
                MaxDepth = MAX_DEPTH,
                Formatting = Formatting.None
            };
            this._jObject = value;
        }

        public JsonNetResult(object value, JsonSerializerSettings serializerSettings)
            : base(value, serializerSettings) {
            this._jObject = value;
        }

        public override string ToString() {
            return JsonConvert.SerializeObject(_jObject, Formatting.None,
                   new JsonSerializerSettings {
                       DateFormatString = DATE_TIME_FORMATE,
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       StringEscapeHandling = StringEscapeHandling.EscapeHtml,
                   });
        }
    }
}
