﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Helpers {
    public class AuthOptions {
        public const string ISSUER = "ManageMikrotik"; // издатель токена
        public const string AUDIENCE = "ManageMikotikApp"; // потребитель токена
        const string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации
        public const int LIFETIME = 180; // время жизни токена - 180 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey() {
            return new SymmetricSecurityKey(System.Text.Encoding.ASCII.GetBytes(KEY));
        }
    }
}
