﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using ManageMikrotikApp.UnitOfWork.Abstract;
using ManageMikrotikApp.Web.Helpers;
using ManageMikrotikApp.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        IBaseObjectService<User> userService => (IBaseObjectService<User>)serviceManager.GetService<User>();

        IUserFacade _userFacade;

        public UserController(IUnitOfWorkFactory ufwFactory,
                              IUserFacade userFacade,
                              IServiceManager serviceManager) 
            : base(ufwFactory, serviceManager) {
            _userFacade = userFacade;
        }
        [HttpGet]
        public IActionResult GetList([FromQuery]PaginateModel paginateModel) {
            return GetResult<PaginateResultModel<UserViewModel>>(() => {
                return _userFacade.GetDataForGridAsync(paginateModel, "/api/User/");
            });
        }
        
        [HttpGet("{id}")]
        public IActionResult Get(int id) {
            return GetResult<UserViewModel>(() => {
                return _userFacade.Get(id);
            });
        }

        [HttpPost]
        public IActionResult Update([FromBody]UserViewModel userModel) {
            return GetResult<UserViewModel>(() => {
                return _userFacade.AddOrUpdate(userModel);
            });
        }

        public IActionResult UpdatePassword([FromBody]UserViewModel userModel) {
            return GetResult<UserViewModel>(() => {
                return _userFacade.AddOrUpdate(userModel);
            });
        }

        [HttpPut]
        public IActionResult Create([FromBody]UserViewModel userModel) {
            return GetResult<UserViewModel>(() => {
                return _userFacade.AddOrUpdate(userModel);
            });
        }
        [HttpDelete]
        public IActionResult Delete([FromQuery]int id) {
            return GetResult<object>(() => {
                using (IUnitOfWork ufw = ufwFactory.Create()) {
                    userService.Delete(ufw, user => user.Id == id);
                    return ufw.SaveChanges();
                }
            });
        }
    }
}