﻿using ManageMikrotikApp.BLL;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using ManageMikrotikApp.UnitOfWork.Abstract;
using ManageMikrotikApp.Web.Helpers;
using ManageMikrotikApp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Controllers {
    
    public class AccountController  : BaseController {
        //private IBaseRepository<User> userRep => ufw.GetRepository<User>();
        public AccountController(IUnitOfWorkFactory ufwFactory) 
            : base(ufwFactory) {

        }
        [HttpPost("/token")]
        public ActionResult Login([FromBody]LoginModel loginModel) {
            try {
                var username = loginModel?.username;
                var password = loginModel.password;
                var identity = GetIdentity(username, password);
                if (identity == null) {
                    //Response.StatusCode = 401;
                    return new JsonNetResult(new AnswerModel<object> {
                        Success = false,
                        Message = "Неверное имя пользователя или пароль"
                    });
                }
                var now = DateTime.UtcNow;
                // создаем JWT-токен
                var jwt = new JwtSecurityToken(
                        issuer: AuthOptions.ISSUER,
                        audience: AuthOptions.AUDIENCE,
                        notBefore: now,
                        claims: identity.Claims,
                        expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var res = new AnswerModel<object> {
                    Success = true,
                    Obj = new TokenInfo { Token = encodedJwt, Username = username }
                };
                return new JsonNetResult(res);
            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
                return new JsonNetResult(new AnswerModel<object> {
                    Success = false,
                    Message = "Ошибка получения токена"
                });
            }
        }

        private ClaimsIdentity GetIdentity(string username, string password) {
            using (IUnitOfWork ufw = ufwFactory.Create()) {
                var userRep = ufw.GetRepository<User>();
                var user = userRep.Get(u => u.Nik == username).FirstOrDefault();
                bool? isAdmin = user?.IsAdmin;
                if (user is null || isAdmin.HasValue && !isAdmin.Value) {
                    return null;
                }

                using (PasswordCryptographer passwordCryptographer = new PasswordCryptographer()) {
                    bool isEqual = passwordCryptographer.AreEqual(user.Password, password);
                    string pass = passwordCryptographer.GenerateSaltedPassword("qweasd");
                    if (!isEqual)
                        return null;
                    var claims = new List<Claim> {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Nik)
                };
                    ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                    return claimsIdentity;
                }
            }
        }
    }
}
