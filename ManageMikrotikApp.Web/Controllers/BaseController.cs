﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using ManageMikrotikApp.Web.Helpers;
using ManageMikrotikApp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Web.Controllers {
    public delegate T GetResultModelForController<T>();
    public class BaseController : Controller {
        protected IUnitOfWorkFactory ufwFactory;
        protected IServiceManager serviceManager;

        public BaseController(IUnitOfWorkFactory ufwFactory) {
            this.ufwFactory = ufwFactory;
        }

        public BaseController(IUnitOfWorkFactory ufwFactory,
                              IServiceManager serviceManager)
            :this(ufwFactory){
            this.serviceManager = serviceManager;
        }

        protected virtual IActionResult GetResult<T>(GetResultModelForController<T> predicateGetModel) {
            var resModel = new AnswerModel<T>();
            try {
                var model = predicateGetModel();
                resModel.Success = true;
                resModel.Obj = model;
            }
            catch (Exception e) {
                resModel.Message = e.Message;
            }
            return new JsonNetResult(resModel);
        }
    }
}
