﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.PresenationLayer.Services.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using ManageMikrotikApp.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]    
    public class StandardController : BaseController {

        IViewModelService _viewModelService => (IViewModelService)serviceManager.GetNoDbService<IViewModelService>();

        public StandardController(IUnitOfWorkFactory ufwFactory, IServiceManager serviceManager) 
            : base(ufwFactory, serviceManager) {
        }
        [HttpGet("GetNewViewModel/{viewModelName}")]
        public IActionResult GetNewViewModel(string viewModelName) {            
            return new JsonNetResult(_viewModelService.GetViewModel(viewModelName));
        }
    }
}