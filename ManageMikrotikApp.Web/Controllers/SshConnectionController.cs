﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SshConnectionController : BaseController
    {
        //IBaseObjectService<SshConnection> sshConnectionService => (IBaseObjectService<SshConnection>)serviceManager.GetService<SshConnection>();

        ISshConnectionFacade _sshConnectionFacade;

        public SshConnectionController(IUnitOfWorkFactory ufwFactory, 
                                       IServiceManager serviceManager,
                                       ISshConnectionFacade sshConnectionFacade) 
            : base(ufwFactory, serviceManager) {
            _sshConnectionFacade = sshConnectionFacade;
        }

        [HttpGet]
        public IActionResult GetList([FromQuery]PaginateModel paginateModel) {
            return GetResult(() => {
                return _sshConnectionFacade.GetDataForGridAsync(paginateModel, "/api/SshConnection");
                //using (IUnitOfWork ufw = ufwFactory.Create()) {
                //    return sshConnectionService.GetDataForGridAsync(ufw, paginateModel, "/api/SshConnection");
                //}
            });
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id) {
            return GetResult(() => {
                return _sshConnectionFacade.Get(id);
                //using (IUnitOfWork ufw = ufwFactory.Create()) {
                //    return sshConnectionService.Get(ufw, id);
                //}
            });
        }
        [HttpPost]
        public IActionResult Update([FromBody]SshConnection sshConnection) {
            return GetResult(() => {
                return _sshConnectionFacade.AddOrUpdate(sshConnection);
                //using (IUnitOfWork ufw = ufwFactory.Create()) {
                //    return sshConnectionService.Update(ufw, sshConnection);
                //}
            });
        }
        [HttpPut]
        public IActionResult Create([FromBody]SshConnection sshConnection) {
            return GetResult(() => {
                return _sshConnectionFacade.AddOrUpdate(sshConnection);
                //using (IUnitOfWork ufw = ufwFactory.Create()) {
                //    return sshConnectionService.Append(ufw, sshConnection);
                //}
            });
        }
        [HttpDelete]
        public IActionResult Delete([FromQuery]int id) {
            return GetResult<int>(() => {
                return _sshConnectionFacade.Delete(x => x.Id == id);
                //using (IUnitOfWork ufw = ufwFactory.Create()) {
                //    sshConnectionService.Delete(ufw, ssh => ssh.Id == id);
                //    return ufw.SaveChanges();
                //}
            });
        }
    }
}