﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : BaseController {
        private IBaseObjectCrudService<Settings> _settingsService => serviceManager.GetService<Settings>();

        public SettingsController(IUnitOfWorkFactory ufwFactory, 
                                  IServiceManager serviceManager) 
            : base(ufwFactory, serviceManager) {
        }

        [HttpGet]
        public IActionResult GetList() {
            return GetResult(() => {
                using(IUnitOfWork ufw = ufwFactory.Create()) {
                    return _settingsService.GetAll(ufw);
                }
            });
        }
        [HttpPost]
        public IActionResult Update([FromBody]List<Settings> settings) {
            return GetResult(() => {
                using (IUnitOfWork ufw = ufwFactory.Create()) {
                    var config = new AutoMapper.MapperConfiguration(c => {
                        c.CreateMap<Settings, Settings>();
                    });
                    config.AssertConfigurationIsValid();
                    var mapper = config.CreateMapper();

                    settings.ForEach(setting => {
                        var set = _settingsService.Get(ufw, setting.Id);
                        set = mapper.Map(setting, set);
                        _settingsService.Update(ufw, set);                        
                    });
                    return ufw.SaveChanges();
                }
            });
        }
    }
}