﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IUnitOfWorkFactory ufwFactory, 
            IServiceManager serviceManager) 
            : base(ufwFactory, serviceManager) {
        }

        public IActionResult Index() {
            return View();
        }
        public IActionResult Error() {
            return View();
        }
    }
}