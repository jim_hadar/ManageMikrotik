﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ManageMikrotikApp.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ScriptController : BaseController
    {
        private IBaseObjectService<Script> scriptService => (IBaseObjectService<Script>)serviceManager.GetService<Script>();

        IScriptFacade _scriptFacade;

        public ScriptController(IUnitOfWorkFactory ufwFactory, 
                                IServiceManager serviceManager, 
                                IScriptFacade scriptFacade) 
            : base(ufwFactory, serviceManager) {
            _scriptFacade = scriptFacade;
        }

        [HttpGet]
        public IActionResult GetList([FromQuery]PaginateModel paginateModel) {
            return GetResult(() => {
                return _scriptFacade.GetDataForGridAsync(paginateModel, "/api/Script");
            });
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id) {
            return GetResult(() => {
                return _scriptFacade.Get(id);
            });
        }
        [HttpPost]
        public IActionResult Update([FromBody]ScriptViewModel viewModel) {
            return GetResult(() => {
                return _scriptFacade.AddOrUpdate(viewModel);
            });
        }
        [HttpPut]
        public IActionResult Create([FromBody]ScriptViewModel viewModel) {
            return GetResult(() => {
                return _scriptFacade.AddOrUpdate(viewModel);
            });
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id) {
            return GetResult(() => {
                return _scriptFacade.Delete(s => s.Id == id);
            });
        }
    }
}