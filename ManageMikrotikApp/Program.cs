﻿using ManageMikrotikApp.Abstractions;
using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.BLL.Services.Concrete;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Concrete;
using ManageMikrotikApp.Common.Ssh;
using ManageMikrotikApp.DAL.PostgreSQL.UnitOfWork;
using ManageMikrotikApp.NoDb.Services.Concrete;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace ManageMikrotikApp {
    class Program {
        static void Main(string[] args) {
            //StartAppWithConfigFile(args);
            StartAppWithDb(args);
        }

        static string GetConfigFilePath(string[] args,
                                        string defaultConfigFile) {
#if DEBUG
            return Path.Combine(Directory.GetCurrentDirectory(), defaultConfigFile);
#else
            string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            //once you have the path you get the directory with:
            var directory = System.IO.Path.GetDirectoryName(path).Replace("file:", string.Empty);
            if (args.Length > 0) {
                if (File.Exists(args[0])) {
                    directory = Path.GetDirectoryName(args[0]);
                    defaultConfigFile = Path.GetFileName(args[0]);
                }
                else {
                    new Exception($"file {args[0]} not found");
                }
            }

            if (directory.Substring(0, 1) == @"\") {
                directory = directory.Substring(1);
            }

            var configFilePath = Path.Combine(directory, defaultConfigFile);

            Console.WriteLine("ConfigFilePath: {0}", configFilePath);
            return configFilePath;
#endif
        }

        static void StartAppWithConfigFile(string[] args) {
            string configFile = "appsettings.manyhosts.json";
            var configFilePath = GetConfigFilePath(args, configFile);

            ISshServiceFactory sshServiceFactory = new SshServiceFactory();

            List<IManageMikrotikService> services = new List<IManageMikrotikService>();
            services.Add(new TelegramBotServiceNoDb(sshServiceFactory, configFilePath));
            services.Add(new NetpingManageMikrotikServiceNoDb(sshServiceFactory, configFilePath));

            services.ForEach(service => service.Start());

            try {
                while (true) {
                    Thread.Sleep(3000);
                }
            }
            catch (Exception e) {

            }
            finally {
                Console.WriteLine("Application is end");
                services.ForEach(service => service.Stop());
            }
        }

        static void StartAppWithDb(string[] args) {
            string configFile = "appconfig.json";
            string directory = Directory.GetCurrentDirectory();
            var configPath = GetConfigFilePath(args, configFile);

            IUnitOfWorkFactory ufwFactory = new UnitOfWorkFactory(configPath);
            IServiceManagerFactory serviceManagerFactory = new ServiceManagerFactory();
            var serviceManager = serviceManagerFactory.Create();
            ISshServiceFactory sshServiceFactory = new SshServiceFactory();
            IManageMikrotikService telegramBotServiceDb = new TelegramBotServiceDb(sshServiceFactory, ufwFactory, serviceManager);
            IManageMikrotikService netpingServiceDb = new NetpingManageMikrotikServiceDb(sshServiceFactory, ufwFactory, serviceManager);

            telegramBotServiceDb.Start();
            netpingServiceDb.Start();

#if DEBUG
            while (Console.ReadLine() != "q") {

            }
#else
            try {
                while (true) {
                    Thread.Sleep(3000);
                }
            }
            catch (Exception e) {

            }
            finally {
                Console.WriteLine("Application is end");
                telegramBotServiceDb.Stop();
                netpingServiceDb.Stop();
            }
#endif
        }
    }
}