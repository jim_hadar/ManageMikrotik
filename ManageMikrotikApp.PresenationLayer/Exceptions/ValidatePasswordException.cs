﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Exceptions {
    public class ValidatePasswordException : Exception {
        public ValidatePasswordException()
            :base ("Ошибка проверки пароля"){
        }

        public ValidatePasswordException(string message) : base(message) {
        }
    }
}
