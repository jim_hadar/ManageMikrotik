﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Services.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Services.Concrete {
    public class ViewModelServiceFactory : IViewModelServiceFactory {
        public IViewModelService Create() {
            IViewModelService service = new ViewModelService();
            #region register viewModels
            service.RegisterViewModel<UserViewModel>(nameof(UserViewModel));
            #endregion

            #region register models
            service.RegisterModel<SshConnection>(nameof(SshConnection));
            service.RegisterModel<Router>(nameof(Router));
            service.RegisterModel<CommandToExecute>(nameof(CommandToExecute));
            service.RegisterModel<Script>(nameof(Script));
            #endregion
            return service;
        }
    }
}
