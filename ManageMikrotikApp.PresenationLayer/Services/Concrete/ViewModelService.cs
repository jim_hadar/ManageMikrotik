﻿using ManageMikrotikApp.BLL.Services.Db.Concrete;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.PresenationLayer.Services.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Services.Concrete {
    public class ViewModelService : BaseService, IViewModelService {
        private Dictionary<string, Type> _viewModels;
        public ViewModelService() {
            _viewModels = new Dictionary<string, Type>();
        }
        public dynamic GetViewModel(string viewModelName) {
            return Activator.CreateInstance(_viewModels[viewModelName]);
        }

        public void RegisterViewModel<T>(string viewModelName) where T : IBaseViewModel {
            if (!_viewModels.ContainsKey(viewModelName)) {
                _viewModels.Add(viewModelName, typeof(T));
            }
        }

        public void RegisterModel<T>(string modelName) where T : IBaseObject {
            if (!_viewModels.ContainsKey(modelName)) {
                _viewModels.Add(modelName, typeof(T));
            }
        }
    }
}
