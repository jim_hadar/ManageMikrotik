﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Concrete;
using ManageMikrotikApp.PresenationLayer.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Services.Concrete {
    public class ServiceManagerPresentationFactory : ServiceManagerFactory {
        public override IServiceManager Create() {
            IServiceManager serviceManager = base.Create();
            var viewModelServiceFactory = new ViewModelServiceFactory();
            serviceManager.RegisterNoDbService(viewModelServiceFactory.Create());
            return serviceManager;
        }
    }
}
