﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Services.Abstract {
    public interface IViewModelService : IService {
        dynamic GetViewModel(string viewModelName);

        void RegisterViewModel<T>(string viewModelName) where T : IBaseViewModel;

        void RegisterModel<T>(string modelName) where T : IBaseObject;
    }
}
