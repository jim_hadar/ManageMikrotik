﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Services.Abstract {
    public interface IViewModelServiceFactory {
        IViewModelService Create();
    }
}
