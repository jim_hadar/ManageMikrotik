﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.ViewModels {
    public class UserViewModel : IBaseObject, IBaseViewModel {
        public int Id { get; set; }
        public string Nik { get; set; }
        public string Phone { get; set; }
        public string Fio { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string NewPasswordConfirm { get; set; }
        public bool IsAdmin { get; set; }
        public string IsAdminString => IsAdmin ? "да" : "нет";
    }
}
