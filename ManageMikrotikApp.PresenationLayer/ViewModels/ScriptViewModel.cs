﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.ViewModels {
    public class ScriptViewModel : Script, IBaseViewModel {
        public ICollection<User> Users { get; set; }
    }
}
