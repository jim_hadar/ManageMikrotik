﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Abstract {
    public interface IUserFacade : IFacade<User, UserViewModel> {
        /// <summary>
        /// валидация пароля для сохранения
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        bool ValidatePassword(IUnitOfWork ufw, UserViewModel userViewModel);
    }
}
