﻿using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Abstract {
    public interface IFacade<TEntity, TViewModel> : IService
        where TEntity : IBaseObject, new()
        where TViewModel : IBaseObject{
        TViewModel CreateViewModel();
        TViewModel AddOrUpdate(TViewModel viewModel);
        TViewModel Get(int id);
        IQueryable<TViewModel> Get(Func<TEntity, bool> predicate);
        PaginateResultModel<TViewModel> GetDataForGridAsync(PaginateModel paginateModel, string loadUrl);
        int Delete(Func<TEntity, bool> predicate);
    }
}
