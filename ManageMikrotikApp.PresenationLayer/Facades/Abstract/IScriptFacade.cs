﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Abstract {
    public interface IScriptFacade : IFacade<Script, ScriptViewModel> {

    }
}
