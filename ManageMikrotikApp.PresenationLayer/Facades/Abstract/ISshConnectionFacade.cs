﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Abstract {
    public interface ISshConnectionFacade : IFacade<SshConnection, SshConnection> {
        (string, bool) ValidateConnection(IUnitOfWork ufw, SshConnection connection);
    }
}
