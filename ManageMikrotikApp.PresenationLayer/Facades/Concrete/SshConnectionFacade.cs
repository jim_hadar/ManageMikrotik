﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Concrete {
    public class SshConnectionFacade : BaseFacade<SshConnection, SshConnection>, ISshConnectionFacade {
        IBaseObjectService<SshConnection> sshConnectionService => (IBaseObjectService<SshConnection>)_serviceManager.GetService<SshConnection>();
        public SshConnectionFacade(IServiceManager serviceManager, 
                                   IUnitOfWorkFactory ufwFactory) 
            : base(serviceManager, ufwFactory) {
        }

        public (string, bool) ValidateConnection(IUnitOfWork ufw, SshConnection connection) {
            StringBuilder resMsgBilder = new StringBuilder();
            var connInBase = sshConnectionService.Get(ufw, conn => conn.HostName == connection.HostName).FirstOrDefault();
            if(connInBase != null && connInBase.Id != connection.Id) {
                resMsgBilder.AppendLine($"Параметры соединения с HostName = {connection.HostName} уже существуют");
            }
            if(connection.Port <= 0 || connection.Port > 65535) {
                resMsgBilder.AppendLine($"Порт {connection.Port} имеет недопустимое значение");
            }
            if (string.IsNullOrEmpty(connection.Username)) {
                resMsgBilder.AppendLine("Укажите имя пользователя");
            }
            var resMsg = resMsgBilder.ToString();
            if(!string.IsNullOrEmpty(resMsg))
                resMsg = resMsg.Remove(resMsg.Length - 1, 1);

            return (resMsg, string.IsNullOrEmpty(resMsg));
        }

        protected override SshConnection GetEntityForSave(IUnitOfWork ufw, SshConnection viewModel) {
            (string msg, bool isValid) = ValidateConnection(ufw, viewModel);
            if(!isValid) {
                throw new Exception(msg);
            }
            var baseRes = base.GetEntityForSave(ufw, viewModel);
            if(baseRes is null) {
                baseRes = viewModel;
            }
            return baseRes;
        }

        protected override SshConnection GetViewModel(SshConnection entity) {
            return entity;
        }
    }
}
