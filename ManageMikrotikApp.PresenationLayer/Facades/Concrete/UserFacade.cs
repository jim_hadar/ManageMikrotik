﻿using ManageMikrotikApp.BLL;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Exceptions;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Concrete {
    public class UserFacade : BaseFacade<User, UserViewModel>, IUserFacade {
        public UserFacade(IServiceManager serviceManager, IUnitOfWorkFactory ufwFactory) 
            : base(serviceManager, ufwFactory) {

        }

        public bool ValidatePassword(IUnitOfWork ufw, UserViewModel userViewModel) {
            var user = entityService.Get(ufw, userViewModel.Id);
            bool res = false;
            //если такой пользователь уже существует
            if(user != null) {
                var crypto = new PasswordCryptographer();
                if(!crypto.AreEqual(user.Password, userViewModel.OldPassword)) {
                    return false;
                }
            }
            //пароли не могут быть пустыми
            if(!string.IsNullOrEmpty(userViewModel.NewPassword) &&
                !string.IsNullOrEmpty(userViewModel.NewPasswordConfirm)) {
                if(userViewModel.NewPassword == userViewModel.NewPasswordConfirm) {
                    res = true;
                }
            }
            return res;
        }

        protected override User GetEntityForSave(IUnitOfWork ufw, UserViewModel viewModel) {
            if (string.IsNullOrEmpty(viewModel.Nik)) {
                throw new Exception("Nik пользователя не может быть пустым");
            }
            var user = entityService.Get(ufw, viewModel.Id);
            var crypto = new PasswordCryptographer();
            if (user is null) {
                user = new User();
                if (viewModel.IsAdmin) {
                    if (!ValidatePassword(ufw, viewModel)) {
                        throw new ValidatePasswordException("Пожалуйста, проверьте правильность ввода пароля");
                    }
                    user.Password = crypto.GenerateSaltedPassword(viewModel.NewPassword);
                }
            }
            else {
                if(string.IsNullOrEmpty(user.Password) && string.IsNullOrEmpty(viewModel.OldPassword) ||
                    !string.IsNullOrEmpty(viewModel.OldPassword)) { 
                    if (!ValidatePassword(ufw, viewModel)) {
                        throw new ValidatePasswordException("Пожалуйста, проверьте правильность ввода пароля");
                    }
                    user.Password = crypto.GenerateSaltedPassword(viewModel.NewPassword);
                }
            }
            user.Fio = viewModel.Fio;
            user.IsAdmin = viewModel.IsAdmin;
            user.Nik = viewModel.Nik;
            user.Phone = viewModel.Phone;
            return user;
        }

        protected override UserViewModel GetViewModel(User entity) {
            var viewModel = new UserViewModel {
                Id = entity.Id,
                Fio = entity.Fio,
                IsAdmin = entity.IsAdmin,
                Nik = entity.Nik,
                Phone = entity.Phone
            };
            return viewModel;
        }
    }
}
