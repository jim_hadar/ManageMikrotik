﻿using AutoMapper;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.PresenationLayer.ViewModels;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Concrete {
    public class ScriptFacade : BaseFacade<Script, ScriptViewModel>, IScriptFacade {
        IBaseObjectService<Router> routerService => (IBaseObjectService<Router>)_serviceManager.GetService<Router>();
        IBaseObjectService<CommandToExecute> commandToExecuteService => (IBaseObjectService<CommandToExecute>)_serviceManager.GetService<CommandToExecute>();
        IBaseObjectCrudService<ExecuteScriptUsers> executeScriptUsersService => _serviceManager.GetService<ExecuteScriptUsers>();
        public ScriptFacade(IServiceManager serviceManager, 
                            IUnitOfWorkFactory ufwFactory) 
            : base(serviceManager, ufwFactory) {
        }

        public override ScriptViewModel AddOrUpdate(ScriptViewModel viewModel) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                //удаляем все записи о правах
                executeScriptUsersService.Delete(ufw, x => x.ScriptId == viewModel.Id);
                //удаляем роутеры, которых нет в новой конфигурации
                var scriptRouters = routerService.Get(ufw, r => r.ScriptId == viewModel.Id);
                foreach (var scriptRouter in scriptRouters) {
                    if (viewModel.Router.FirstOrDefault(r => r.Id == scriptRouter.Id) is null) {
                        routerService.Delete(ufw, r => r.Id == scriptRouter.Id);
                    }
                }
                var entity = GetEntityForSave(ufw, viewModel);

                var script = base.AddOrUpdate(ufw, viewModel);

                foreach (var user in viewModel.Users) {
                    var exUsers = new ExecuteScriptUsers {
                        ScriptId = script.Id,
                        UserId = user.Id
                    };
                    executeScriptUsersService.Append(ufw, exUsers);
                }                

                foreach (var viewRouter in viewModel.Router) {
                    commandToExecuteService.Delete(ufw, c => c.RouterId == viewRouter.Id);
                    var router = routerService.Get(ufw, viewRouter.Id);
                    if(router is null) {
                        router = new Router();
                    }
                    router.ScriptId = script.Id;
                    router.Name = viewRouter.Name;
                    router.SshConnectionId = viewRouter.SshConnection.Id;
                    if (viewRouter.Id <= 0) {
                        routerService.Append(ufw, router);
                    }
                    else {
                        routerService.Update(ufw, router);
                    }

                    ufw.SaveChanges();

                    foreach(var command in viewRouter.CommandToExecute) {
                        commandToExecuteService.Append(ufw, new CommandToExecute {
                            Name = command.Name,
                            Value = command.Value,
                            RouterId = router.Id
                        });
                    }
                    ufw.SaveChanges();
                }
                return GetViewModel(script);
            }
        }

        public override PaginateResultModel<ScriptViewModel> GetDataForGridAsync(PaginateModel paginateModel, string loadUrl) {
            return base.GetDataForGridAsync(paginateModel, loadUrl);
        }

        protected override ScriptViewModel GetViewModel(Script entity) {            
            var viewModel = base.GetViewModel(entity);
            viewModel.Users = new HashSet<User>();
            foreach(var executeScriptUser in entity.ExecuteScriptUsers) {
                viewModel.Users.Add(executeScriptUser.User);
            }
            return viewModel;            
        }

        protected override MapperConfiguration GetMapperConfiguration() {
            MapperConfiguration mapperConfiguration = new AutoMapper.MapperConfiguration((cfg) => {
                cfg.CreateMap<Script, ScriptViewModel>()
                    .ForMember(x => x.Users, opt => opt.Ignore());
                cfg.CreateMap<ScriptViewModel, Script>()
                    .ForMember(x => x.ExecuteScriptUsers, opt => opt.Ignore())
                    .ForMember(x => x.Router, opt => opt.Ignore())
                    .ForMember(x => x.ScriptType, opt => opt.Ignore());
            });
            return mapperConfiguration;
        }

        protected override Script GetEntityForSave(IUnitOfWork ufw, ScriptViewModel viewModel) {
            var entity = base.GetEntityForSave(ufw, viewModel);
            if(entity.Id <= 0) {
                entity.ScriptTypeId = (int)DAL.Abstrasctions.Enums.ScriptType.ExecuteOnRouter;
            }
            //entity.Router = new HashSet<Router>();
            entity.ExecuteScriptUsers = new HashSet<ExecuteScriptUsers>();
            return entity;
        }
    }
}