﻿using AutoMapper;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Concrete;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.PresenationLayer.Facades.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.PresenationLayer.Facades.Concrete {
    public class BaseFacade<TEntity, TViewModel> : BaseService, IFacade<TEntity, TViewModel>
        where TEntity : IBaseObject, new()
        where TViewModel : IBaseObject {
        protected IServiceManager _serviceManager;
        protected IUnitOfWorkFactory _ufwFactory;

        protected IBaseObjectCrudService<TEntity> entityService => _serviceManager.GetService<TEntity>();

        public TViewModel CreateViewModel() {
            return Activator.CreateInstance<TViewModel>();
        }

        public BaseFacade(IServiceManager serviceManager,
                          IUnitOfWorkFactory ufwFactory) {
            _serviceManager = serviceManager;
            this._ufwFactory = ufwFactory;
        }

        public virtual TViewModel AddOrUpdate(TViewModel viewModel) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                return GetViewModel(AddOrUpdate(ufw, viewModel));
            }
        }

        protected virtual TEntity AddOrUpdate(IUnitOfWork ufw, TViewModel viewModel) {            
            var entity = GetEntityForSave(ufw, viewModel);

            if (viewModel.Id <= 0) {
                entityService.Append(ufw, entity);
            }
            else {
                entityService.Update(ufw, entity);
            }

            ufw.SaveChanges();
            return entity;
        }

        public virtual TViewModel Get(int id) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                var entity = entityService.Get(ufw, id);
                return GetViewModel(entity);
            }
        }

        public virtual IQueryable<TViewModel> Get(Func<TEntity, bool> predicate) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                var entities = entityService.Get(ufw, predicate);
                List<TViewModel> viewModels = new List<TViewModel>();
                foreach (var entity in entities) {
                    var viewModel = GetViewModel(entity);
                    viewModels.Add(viewModel);
                }
                return viewModels.AsQueryable();
            }
        }

        public virtual PaginateResultModel<TViewModel> GetDataForGridAsync(PaginateModel paginateModel, string loadUrl) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                if (entityService is IBaseObjectService<TEntity>) {
                    var baseObjectService = entityService as IBaseObjectService<TEntity>;
                    var model = baseObjectService.GetDataForGridAsync(ufw, paginateModel, loadUrl);
                    var viewModels = GetViewModels(model.Data);
                    var res = new PaginateResultModel<TViewModel> {
                        CurrentPage = model.CurrentPage,
                        Data = viewModels,
                        From = model.From,
                        LastPage = model.LastPage,
                        NextPageUrl = model.NextPageUrl,
                        PerPage = model.PerPage,
                        PrevPageUrl = model.PrevPageUrl,
                        To = model.To,
                        Total = model.Total
                    };
                    return res;
                }
                else {
                    throw new NotImplementedException($"сервис не поддерживает операцию");
                }
            }
        }
        public int Delete(Func<TEntity, bool> predicate) {
            using (IUnitOfWork ufw = _ufwFactory.Create()) {
                entityService.Delete(ufw, predicate);
                return ufw.SaveChanges();
            }
        }

        #region abstract methods
        protected virtual TViewModel GetViewModel(TEntity entity) {
            var config = GetMapperConfiguration();
            config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            var viewModel = mapper.Map<TEntity, TViewModel>(entity);
            return viewModel;
        }

        protected virtual TEntity GetEntityForSave(IUnitOfWork ufw, TViewModel viewModel) {
            var entity = entityService.Get(ufw, viewModel.Id);
            if(entity == null) {
                entity = new TEntity();
            }
            if (entity != null) {
                var config = GetMapperConfiguration();
                config.AssertConfigurationIsValid();

                var mapper = config.CreateMapper();
                entity = mapper.Map(viewModel, entity);
            }
            
            return entity;
        }

        protected virtual IQueryable<TViewModel> GetViewModels(IEnumerable<TEntity> entities) {
            var viewModels = new List<TViewModel>();
            foreach(var entity in entities) {
                var viewModel = GetViewModel(entity);
                viewModels.Add(viewModel);
            }
            return viewModels.AsQueryable();
        }        

        protected virtual AutoMapper.MapperConfiguration GetMapperConfiguration() {
            var config = new AutoMapper.MapperConfiguration(c =>
            {
                c.CreateMap<TEntity, TViewModel>();
            });
            return config;
        }

        protected virtual TRes Map<TRes, TModel>(TModel viewModel, MapperConfiguration configuration) {
            var config = configuration;
            config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            var entity = mapper.Map<TRes>(viewModel);
            return entity;
        }

        protected virtual TRes Map<TRes, TModel>(TModel viewModel,
                                                 TRes model,
                                                 MapperConfiguration configuration) {
            var config = configuration;
            config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            model = mapper.Map(viewModel, model);
            return model;
        }
        #endregion
    }
}
