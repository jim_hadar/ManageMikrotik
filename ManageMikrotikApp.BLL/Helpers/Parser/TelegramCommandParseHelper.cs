﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.Common.Services.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.BLL.Helpers.Parser {
    public static class TelegramCommandParseHelper {
        public static (string username, string command, string right) ParseAndSaveAllowRightOnCommand(this ITelegramBotManageService telegramBot, IUnitOfWork ufw, IServiceManager serviceManager, string command) {
            string[] parameters = command.Split(" ");
            if(parameters.Length < 3) {
                throw new Exception("недостаточно параметров для назначения прав.");
            }
            var errorBuilder = new StringBuilder();

            var userServ = (IUserService<User>)serviceManager.GetService<User>();
            var scriptServ = (IScriptService)serviceManager.GetService<Script>();

            var user = userServ.GetUserByNik(ufw, parameters[1]);

            if(user is null) {
                errorBuilder.AppendLine($"Пользователя с именем <b>{parameters[1]}</b> не найдено");
            }

            var script = scriptServ.Get(ufw, sc => sc.Name.Contains(parameters[2])).FirstOrDefault();

            if(script is null) {
                errorBuilder.AppendLine($"Скрипта с именем <b>{parameters[2]}</b> не найдено");
            }

            if (!string.IsNullOrEmpty(errorBuilder.ToString())) {
                throw new Exception(errorBuilder.ToString());
            }

            var allow = parameters.Length < 4 || parameters.Length >= 4 && parameters[3] == "allow" ? true : false;

            var executeScripsUserServ = serviceManager.GetService<ExecuteScriptUsers>();

            var executeScriptUser = executeScripsUserServ.Get(ufw,
                                            ex => ex.IsAllow == allow &&
                                                    ex.ScriptId == script.Id &&
                                                    ex.UserId == user.Id).FirstOrDefault();
            if (executeScriptUser is null) {
                executeScriptUser = new ExecuteScriptUsers {
                    IsAllow = allow,
                    ScriptId = script.Id,
                    UserId = user.Id
                };
                executeScripsUserServ.Append(ufw, executeScriptUser);
            }
            else {
                executeScriptUser.IsAllow = allow;
                executeScriptUser.ScriptId = script.Id;
                executeScriptUser.UserId = script.Id;
                executeScripsUserServ.Update(ufw, executeScriptUser);
            }           

            ufw.SaveChanges();

            return (parameters[1], parameters[2], allow ? "allow" : "deny");
        }
    }
}