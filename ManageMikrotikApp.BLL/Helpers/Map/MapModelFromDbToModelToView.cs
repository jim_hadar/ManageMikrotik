﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ManageMikrotikApp.BLL.Helpers.Map {
    public static class MapModelFromDbToModelToView {
        /// <summary>
        /// map script info from Database to common model
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public static Abstractions.Models.Script MapScriptFromDbToModel(Script script) {
            var scriptResult = new Abstractions.Models.Script {
                ScriptName = script.Name,
                Description = script.Description                
            };
            script.Router?.ToList()?.ForEach(router => {
                scriptResult.Routers.Add(MapRouterInfo(router));
            });
            return scriptResult;
        }
        /// <summary>
        /// map router from Database to common model
        /// </summary>
        /// <param name="routerFromDb"></param>
        /// <returns></returns>
        public static Abstractions.Models.Router MapRouterInfo(Router routerFromDb) {
            var routerToResult = new Abstractions.Models.Router {
                RouterName = routerFromDb.Name
            };
            routerToResult.CommandsToExecute = MapCommandsToExecuteFromDbForRouter(routerFromDb.CommandToExecute);
            routerToResult.SshConnectionInfo = MapSshConnectionForRouter(routerFromDb.SshConnection);
            return routerToResult;
        }
        /// <summary>
        /// map sshConnectionInfo to common model
        /// </summary>
        /// <param name="sshConnectionFromDb"></param>
        /// <returns></returns>
        public static Abstractions.Models.SshConnectionInfo MapSshConnectionForRouter(SshConnection sshConnectionFromDb) {
            var sshConnectionInfoResult = new Abstractions.Models.SshConnectionInfo {
                Host = sshConnectionFromDb?.HostName,
                Port = sshConnectionFromDb?.Port ?? 0,
                UserName = sshConnectionFromDb?.Username,
                Password = sshConnectionFromDb?.Password
            };
            return sshConnectionInfoResult;
        }        
        /// <summary>
        /// map collection of commands from Database to List<string>
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        public static List<string> MapCommandsToExecuteFromDbForRouter(ICollection<CommandToExecute> commands) {
            var result = new List<string>();
            if (commands != null) {
                foreach (var command in commands) {
                    result.Add(command.Value);
                }
            }
            return result;
        }
    }
}