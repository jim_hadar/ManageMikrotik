--common.settings
delete from common.settings;

INSERT INTO common.settings(id, name, value)
	VALUES (1, 'Token для telegram бота', '541960143:AAGIfs_97jdFTBHZFrAAIQrsfDeoAup7Otg');

INSERT INTO common.settings(id, name, value)
	VALUES (2, 'Id общего канала для рассылки результатов выполнения команд', '-1001189443121');
	
INSERT INTO common.settings(id, name, value)
	VALUES (3, 'Имя пользователя для авторизации на netping', 'visor');
	
INSERT INTO common.settings(id, name, value)
	VALUES (4, 'Пароль для авторизации на netping', 'ping');

--common.user
delete from common."user";

insert into common."user"(nik, phone, is_admin, Password)
	values ('Jimhadar', '', true, 'xSlpUcBq*Mqk7qYVymxeiO2S6RD5zpQ8bmfBYuHl88rd1cIBQMYIisTOS5uWAR0Y5uxd4ESBSxzNdtoR4RsydV8maQyOwQQ==');
    
-- update common."user"
-- 	set Password = 'xSlpUcBq*Mqk7qYVymxeiO2S6RD5zpQ8bmfBYuHl88rd1cIBQMYIisTOS5uWAR0Y5uxd4ESBSxzNdtoR4RsydV8maQyOwQQ=='
--     where id = 1;

--common.script_type
delete from common.script_type;

insert into common.script_type(id, name, description)
	values (1, 'ExecuteOnRouter', 'ExecuteOnRouter');

insert into common.script_type(id, name, description)
	values (2, 'ExecuteOnServer', 'ExecuteOnServer');

--common.ssh_connection
delete from common.ssh_connection;

-- insert into common.ssh_connection(id, host_name, port, username, password)
-- 	values (1, '192.168.1.101', 22, 'admin', '');

-- insert into common.ssh_connection(id, host_name, port, username, password)
-- 	values (2, '192.168.1.102', 22, 'admin', '');

-- --common.script
delete from common.script;

-- insert into common.script(id, NAME, description, script_type_id)
-- 	values (1, 'IpInfo', 'ipinfo', 1);

-- insert into common.script(id, NAME, description, script_type_id)
-- 	values (2, 'InterfaceInfo', 'interface info', 1);

-- --common.router
delete from common.router;

-- insert into common.router(id, name, ssh_connection_id, script_id)
-- 	values (1, 'Router1', 1, 1);

-- insert into common.router(id, name, ssh_connection_id, script_id)
-- 	values (2, 'Router2', 2, 1);

-- insert into common.router(id, name, ssh_connection_id, script_id)
-- 	values (3, 'Router1', 1, 2);

-- --common.command_to_execute
delete from common.command_to_execute;

-- insert into common.command_to_execute(id, name, VALUE, router_id)
-- 	values (1, '', '/ip address print', 1);

-- insert into common.command_to_execute(id, name, VALUE, router_id)
-- 	values (2, '', '/ip address print', 2);

-- insert into common.command_to_execute(id, name, VALUE, router_id)
-- 	values (3, '', '/interface print', 3);

delete from common.execute_script_users;

-- insert into common.execute_script_users (id, user_id, script_id, is_allow)
-- 	values (1, 1, 2, true);

