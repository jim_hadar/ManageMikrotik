-- SQL Manager for PostgreSQL 5.7.0.46919
-- ---------------------------------------
-- Хост         : localhost
-- База данных  : manage_mikrotik
-- Версия       : PostgreSQL 10.5, compiled by Visual C++ build 1800, 64-bit

-- drop database if exists manage_mikrotik;

drop table if exists common.execute_script_users;
drop table if exists common."user";
drop table if exists  common.command_to_execute CASCADE;
drop table  if exists common.router;
drop table if exists common.script;
drop table if exists common.script_type;
drop table if exists common.ssh_connection;
drop table if exists common.settings;


drop schema if exists common;

CREATE SCHEMA common AUTHORIZATION postgres;
SET check_function_bodies = false;
--
-- Structure for table user (OID = 16405) : 
--
SET search_path = common, pg_catalog;

CREATE TABLE common."user" (
  id serial NOT NULL,
  nik varchar(200),
  phone varchar(200),
  fio varchar(200),
  password text,
  is_admin boolean NOT NULL
)
WITH (oids = false);
--
-- Structure for table ssh_connection (OID = 16443) : 
--
CREATE TABLE common.ssh_connection (
  id serial NOT NULL,
  host_name varchar NOT NULL,
  port integer NOT NULL,
  username varchar NOT NULL,
  password varchar,
  public_ssh_key varchar
)
WITH (oids = false);
--
-- Structure for table settings (OID = 16465) : 
--
CREATE TABLE common.settings (
  id serial NOT NULL,
  name varchar(300) NOT NULL,
  value varchar NOT NULL
)
WITH (oids = false);
--
-- Structure for table command_to_execute (OID = 16487) : 
--
CREATE TABLE common.command_to_execute (
  id serial NOT NULL,
  name varchar(100),
  value text NOT NULL,
  router_id integer NOT NULL
)
WITH (oids = false);
--
-- Structure for table router (OID = 16499) : 
--
CREATE TABLE common.router (
  id serial NOT NULL,
  name varchar(200) NOT NULL,
  ssh_connection_id integer,
  script_id integer
)
WITH (oids = false);
--
-- Structure for table script (OID = 16573) : 
--
CREATE TABLE common.script (
  id serial NOT NULL,
  name varchar(200) NOT NULL,
  description text,
  command_to_execute_id integer,
  script_type_id integer
)
WITH (oids = false);
--
-- Structure for table execute_script_users (OID = 16621) : 
--
CREATE TABLE common.execute_script_users (
  id serial NOT NULL,
  user_id integer,
  script_id integer,
  is_allow boolean DEFAULT true NOT NULL
)
WITH (oids = false);
--
-- Structure for table script_type (OID = 16643) : 
--
CREATE TABLE common.script_type (
  id integer NOT NULL,
  name varchar(200) NOT NULL,
  description text
)
WITH (oids = false);
--
-- Data for table common."user" (OID = 16405) (LIMIT 0,1)
--
INSERT INTO "user" (id, nik, phone, fio, password, is_admin)
VALUES (1, 'Jimhadar', '', NULL, NULL, true);

--
-- Data for table common.ssh_connection (OID = 16443) (LIMIT 0,2)
--
INSERT INTO ssh_connection (id, host_name, port, username, password, public_ssh_key)
VALUES (1, '192.168.1.101', 22, 'admin', '', NULL);

INSERT INTO ssh_connection (id, host_name, port, username, password, public_ssh_key)
VALUES (2, '192.168.1.102', 22, 'admin', '', NULL);

--
-- Data for table common.settings (OID = 16465) (LIMIT 0,2)
--
INSERT INTO settings (id, name, value)
VALUES (1, 'AuthentificationToken', '541960143:AAGIfs_97jdFTBHZFrAAIQrsfDeoAup7Otg');

INSERT INTO settings (id, name, value)
VALUES (2, 'CommonChannelId', '-1001189443121');

--
-- Data for table common.command_to_execute (OID = 16487) (LIMIT 0,3)
--
INSERT INTO command_to_execute (id, name, value, router_id)
VALUES (1, '', '/ip address print', 1);

INSERT INTO command_to_execute (id, name, value, router_id)
VALUES (2, '', '/ip address print', 2);

INSERT INTO command_to_execute (id, name, value, router_id)
VALUES (3, '', '/interface print', 3);

--
-- Data for table common.router (OID = 16499) (LIMIT 0,3)
--
INSERT INTO router (id, name, ssh_connection_id, script_id)
VALUES (1, 'Router1', 1, 1);

INSERT INTO router (id, name, ssh_connection_id, script_id)
VALUES (2, 'Router2', 2, 1);

INSERT INTO router (id, name, ssh_connection_id, script_id)
VALUES (3, 'Router1', 1, 2);

--
-- Data for table common.script (OID = 16573) (LIMIT 0,2)
--
INSERT INTO script (id, name, description, command_to_execute_id, script_type_id)
VALUES (1, 'IpInfo', 'ipinfo', NULL, 1);

INSERT INTO script (id, name, description, command_to_execute_id, script_type_id)
VALUES (2, 'InterfaceInfo', 'interface info', NULL, 1);

--
-- Data for table common.execute_script_users (OID = 16621) (LIMIT 0,2)
--
INSERT INTO execute_script_users (id, user_id, script_id, is_allow)
VALUES (1, 1, 2, true);

INSERT INTO execute_script_users (id, user_id, script_id, is_allow)
VALUES (2, 1, 1, true);

--
-- Data for table common.script_type (OID = 16643) (LIMIT 0,2)
--
INSERT INTO script_type (id, name, description)
VALUES (1, 'ExecuteOnRouter', 'ExecuteOnRouter');

INSERT INTO script_type (id, name, description)
VALUES (2, 'ExecuteOnServer', 'ExecuteOnServer');

--
-- Definition for index router_pkey (OID = 16512) : 
--
ALTER TABLE ONLY router
  ADD CONSTRAINT router_pkey
PRIMARY KEY (id);
--
-- Definition for index User_pkey (OID = 16521) : 
--
ALTER TABLE ONLY "user"
  ADD CONSTRAINT "User_pkey"
PRIMARY KEY (id);
--
-- Definition for index ssh_connection_pkey (OID = 16531) : 
--
ALTER TABLE ONLY ssh_connection
  ADD CONSTRAINT ssh_connection_pkey
PRIMARY KEY (id);
--
-- Definition for index settings_pkey (OID = 16541) : 
--
ALTER TABLE ONLY settings
  ADD CONSTRAINT settings_pkey
PRIMARY KEY (id);
--
-- Definition for index command_to_execute_pkey (OID = 16561) : 
--
ALTER TABLE ONLY command_to_execute
  ADD CONSTRAINT command_to_execute_pkey
PRIMARY KEY (id);
--
-- Definition for index command_pkey (OID = 16580) : 
--
ALTER TABLE ONLY script
  ADD CONSTRAINT command_pkey
PRIMARY KEY (id);
--
-- Definition for index router_script_fk (OID = 16592) : 
--
ALTER TABLE ONLY router
  ADD CONSTRAINT router_script_fk
FOREIGN KEY (script_id) REFERENCES script(id) ON DELETE CASCADE;
--
-- Definition for index command_to_execute_router_fk (OID = 16597) : 
--
ALTER TABLE ONLY command_to_execute
  ADD CONSTRAINT command_to_execute_router_fk
FOREIGN KEY (router_id) REFERENCES router(id) ON DELETE CASCADE;
--
-- Definition for index execute_command_users_pkey (OID = 16626) : 
--
ALTER TABLE ONLY execute_script_users
  ADD CONSTRAINT execute_command_users_pkey
PRIMARY KEY (id);
--
-- Definition for index command_type_pkey (OID = 16649) : 
--
ALTER TABLE ONLY script_type
  ADD CONSTRAINT command_type_pkey
PRIMARY KEY (id);
--
-- Definition for index script_command_to_exec_fk (OID = 16656) : 
--
ALTER TABLE ONLY script
  ADD CONSTRAINT script_command_to_exec_fk
FOREIGN KEY (command_to_execute_id) REFERENCES command_to_execute(id);
--
-- Definition for index script_script_type_fk (OID = 16666) : 
--
ALTER TABLE ONLY script
  ADD CONSTRAINT script_script_type_fk
FOREIGN KEY (script_type_id) REFERENCES script_type(id) ON DELETE SET NULL;
--
-- Definition for index router_ssh_connection_fk (OID = 16676) : 
--
ALTER TABLE ONLY router
  ADD CONSTRAINT router_ssh_connection_fk
FOREIGN KEY (ssh_connection_id) REFERENCES ssh_connection(id) ON UPDATE SET NULL ON DELETE SET NULL;
--
-- Definition for index execute_script_users_user_fk (OID = 16681) : 
--
ALTER TABLE ONLY execute_script_users
  ADD CONSTRAINT execute_script_users_user_fk
FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE SET NULL;
--
-- Definition for index execute_script_users_command_fk (OID = 16687) : 
--
ALTER TABLE ONLY execute_script_users
  ADD CONSTRAINT execute_script_users_command_fk
FOREIGN KEY (script_id) REFERENCES script(id) ON DELETE SET NULL;
--
-- Data for sequence common."User_id_seq" (OID = 16403)
--
-- SELECT pg_catalog.setval('"User_id_seq"', 9, true);
-- --
-- -- Data for sequence common.ssh_connection_id_seq (OID = 16441)
-- --
-- SELECT pg_catalog.setval('ssh_connection_id_seq', 9, true);
-- --
-- -- Data for sequence common.settings_id_seq (OID = 16463)
-- --
-- SELECT pg_catalog.setval('settings_id_seq', 1, false);
-- --
-- -- Data for sequence common.command_to_execute_id_seq (OID = 16485)
-- --
-- SELECT pg_catalog.setval('command_to_execute_id_seq', 1, false);
-- --
-- -- Data for sequence common.router_id_seq (OID = 16497)
-- --
-- SELECT pg_catalog.setval('router_id_seq', 1, false);
-- --
-- -- Data for sequence common.command_id_seq (OID = 16571)
-- --
-- SELECT pg_catalog.setval('command_id_seq', 2, true);
-- --
-- -- Data for sequence common.execute_command_users_id_seq (OID = 16619)
-- --
-- SELECT pg_catalog.setval('execute_command_users_id_seq', 2, true);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
COMMENT ON CONSTRAINT router_script_fk ON router IS 'Каждый роутер прикреплен к определенной команде';
COMMENT ON CONSTRAINT command_to_execute_router_fk ON command_to_execute IS 'С каждым роутером связана команда для выполнения на нем.';
COMMENT ON TABLE common.execute_script_users IS 'Связь с пользователя, которым разрешено / запрещено запускать команду';
