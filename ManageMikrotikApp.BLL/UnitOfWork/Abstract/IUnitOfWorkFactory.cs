﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.UnitOfWork.Abstract {
    public interface IUnitOfWorkFactory {
        IUnitOfWork Create();
    }
}
