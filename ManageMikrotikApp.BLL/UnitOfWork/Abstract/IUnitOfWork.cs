﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.UnitOfWork.Abstract {
    public interface IUnitOfWork : IDisposable {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        object DBContext { get; }
        IBaseRepository<TEntity> GetRepository<TEntity>() where TEntity : IBaseObject;
        void RegisterRepository<TEntity>(IBaseRepository repository) where TEntity : IBaseObject;
        void RegisterRepository<TEntity>(IBaseRepository<TEntity> repository) where TEntity : IBaseObject;
        void Commit();
        void Rollback();
    }
}