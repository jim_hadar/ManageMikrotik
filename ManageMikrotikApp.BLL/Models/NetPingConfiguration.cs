﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Models {
    public class NetPingConfiguration {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
