﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Models {
    public class Pagination {
        [JsonProperty("total")]
        public int Total { get; set; }
        [JsonProperty("per_page")]
        public int PerPage { get; set; }
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }
        [JsonProperty("last_page")]
        public int LastPage { get; set; }
        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }
        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }
        [JsonProperty("from")]
        public int From { get; set; }
        [JsonProperty("to")]
        public int To { get; set; }
    }
    
    public class Links {
        [JsonProperty("pagination")]
        public Pagination Pagination { get; set; }
    }
    /// <summary>
    /// ммодель для компонента vuetable-2
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PaginateResultModel<T> where T : IBaseObject {
        [JsonProperty("total")]
        public int Total { get; set; }
        [JsonProperty("per_page")]
        public int PerPage { get; set; }
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }
        [JsonProperty("last_page")]
        public int LastPage { get; set; }
        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }
        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }
        [JsonProperty("from")]
        public int From { get; set; }
        [JsonProperty("to")]
        public int To { get; set; }
        //[JsonProperty("links")]
        //public Links Links { get; set; }
        [JsonProperty("data")]
        public IEnumerable<T> Data { get; set; }
    }
}
