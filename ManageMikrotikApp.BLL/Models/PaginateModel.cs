﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageMikrotikApp.BLL.Models {
    public class PaginateModel {
        /// <summary>
        /// номер страницы
        /// </summary>    
        public int page { get; set; }
        /// <summary>
        /// размер страницы
        /// </summary>
        public int per_page { get; set; }
    }
}
