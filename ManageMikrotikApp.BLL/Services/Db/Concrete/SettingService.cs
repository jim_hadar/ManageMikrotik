﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Enums;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class SettingService : BaseObjectService<Settings>, ISettingService {
        public string GetTelegramBotAuthentificationToken(IUnitOfWork ufw) {
            var result = Get(ufw, (int)SettingsEnum.AuthentificationToken);
            return result.Value;
        }

        public string GetTelegramCommonChannelId(IUnitOfWork ufw) {
            var result = Get(ufw, (int)SettingsEnum.CommonChannelId);
            return result.Value;
        }
    }
}
