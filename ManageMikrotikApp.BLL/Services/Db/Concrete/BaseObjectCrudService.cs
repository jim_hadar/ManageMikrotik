﻿using AutoMapper;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class BaseObjectCrudService<TEntity> : BaseService, IBaseObjectCrudService<TEntity>
        where TEntity : IBaseObject {
        public TEntity Append(IUnitOfWork ufw, TEntity entity) {
            var rep = ufw.GetRepository<TEntity>();
            return rep.Insert(entity);
        }

        public void Delete(IUnitOfWork ufw, Func<TEntity, bool> predicate) {
            var rep = ufw.GetRepository<TEntity>();
            rep.Delete(predicate);
        }

        public IQueryable<TEntity> Get(IUnitOfWork ufw, Func<TEntity, bool> predicate) {
            var rep = ufw.GetRepository<TEntity>();
            return rep.GetAll().Where(predicate).AsQueryable();
        }

        public TEntity Get(IUnitOfWork ufw, int id) {
            var rep = ufw.GetRepository<TEntity>();
            return rep.Get(id);
        }

        public IQueryable<TEntity> GetAll(IUnitOfWork ufw) {
            var rep = ufw.GetRepository<TEntity>();
            return rep.GetAll();
        }

        public TEntity Update(IUnitOfWork ufw, TEntity entity) {
            var rep = ufw.GetRepository<TEntity>();
            return rep.Update(entity);
        }
    }
}