﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using System;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public abstract class BaseService : IService {
        protected bool _disposed = false;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    _disposed = true;
                }
            }
        }
    }
}