﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class ServiceManager : BaseService, IServiceManager {

        Dictionary<Type, IService> _services = new Dictionary<Type, IService>();

        public IService GetNoDbService<TService>() where TService : IService {
            return (IService)_services[typeof(TService)];
        }

        public void RegisterNoDbService<TService>() where TService : IService, new() {
            RegisterNoDbService(new TService());
        }

        public void RegisterNoDbService<TService>(TService service) where TService : IService {
            if (!_services.ContainsKey(typeof(TService))) {
                _services.Add(typeof(TService), service);
            }
        }

        public IBaseObjectCrudService<TEntity> GetService<TEntity>() where TEntity : IBaseObject {
            return (IBaseObjectCrudService<TEntity>)_services[typeof(TEntity)];
        }        

        public void RegisterService<TEntity>(IBaseObjectCrudService<TEntity> service) 
            where TEntity : IBaseObject {
            if (!_services.ContainsKey(typeof(TEntity))) {
                _services.Add(typeof(TEntity), service);
            }
        }

        public void RegisterService<TEntity>() where TEntity : IBaseObject {
            RegisterService(new BaseObjectService<TEntity>());
        }
    }
}