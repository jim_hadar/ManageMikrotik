﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class BaseObjectCrudServiceFactory : IBaseObjectCrudServiceFactory {
        public IBaseObjectCrudService<TEntity> Create<TEntity>() where TEntity : IBaseObject {
            return new BaseObjectCrudService<TEntity>();
        }
    }
}
