﻿using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class ServiceManagerFactory : IServiceManagerFactory {
        public virtual IServiceManager Create() {
            var serviceManager = new ServiceManager();
            serviceManager.RegisterService<CommandToExecute>();
            serviceManager.RegisterService<ExecuteScriptUsers>();
            serviceManager.RegisterService<Router>();
            serviceManager.RegisterService<Script>(new ScriptService());
            
            serviceManager.RegisterService<ScriptType>();

            serviceManager.RegisterService<Settings>(new SettingService());

            serviceManager.RegisterService<SshConnection>();
            serviceManager.RegisterService<User>(new UserService());
            return serviceManager;
        }
    }
}
