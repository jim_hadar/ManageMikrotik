﻿using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class BaseObjectService<TEntity> : BaseObjectCrudService<TEntity>, IBaseObjectService<TEntity>
        where TEntity : IBaseObject {

        public PaginateResultModel<TEntity> GetDataForGridAsync(IUnitOfWork ufw,
                                                                    PaginateModel paginateModel,
                                                                    string loadUrl) {
            var rep = ufw.GetRepository<TEntity>();
            var entities = rep.GetAllWithoutChild();
            int countTotal = entities.Count();
            entities = entities.Skip((paginateModel.page - 1) * paginateModel.per_page).Take(paginateModel.per_page);

            int currentPage = paginateModel.page;
            int pageSize = paginateModel.per_page;
            int lastPage = (int)Math.Ceiling((double)countTotal / (double)pageSize);
            int nextPage = paginateModel.page + 1;
            int prevPage = paginateModel.page - 1;
            int to = currentPage * pageSize;
            if(to > countTotal) {
                to = countTotal;
            }
            int from = to - pageSize + 1;
            if(from < 0) {
                from = 1;
            }

            var nextPageUrl = nextPage <= lastPage ?
                                    $"{loadUrl}?sort=&per_page={paginateModel.per_page}&page={nextPage}"
                                    : null;
            var prevPageUrl = prevPage > 0 ?
                                    $"{loadUrl}?sort=&per_page={paginateModel.per_page}&page={prevPage}"
                                    : null;

            var pagination = new Pagination {
                CurrentPage = currentPage,
                PerPage = pageSize,
                Total = countTotal,
                LastPage = lastPage,
                NextPageUrl = nextPageUrl,
                PrevPageUrl = prevPageUrl,
                From = from,
                To = to,
            };

            var result = new PaginateResultModel<TEntity> {
                Data = entities,
                //Links = new Links { Pagination = pagination }
                CurrentPage = currentPage,
                PerPage = pageSize,
                Total = countTotal,
                LastPage = lastPage,
                NextPageUrl = nextPageUrl,
                PrevPageUrl = prevPageUrl,
                From = from,
                To = to,
            };
            return result;
        }
    }
}