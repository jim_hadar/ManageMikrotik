﻿using ManageMikrotikApp.BLL.Helpers.Map;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.Common.Exceptions;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class ScriptService : BaseObjectService<Script>, IScriptService {
        public Abstractions.Models.Script GetScriptForExecuteOnRouter(IUnitOfWork ufw, 
                                                                    Func<Script, bool> predicate) {
            var script = Get(ufw, predicate).FirstOrDefault();
            var scriptResult = MapModelFromDbToModelToView.MapScriptFromDbToModel(script);
            return scriptResult;
        }

        public Abstractions.Models.Script GetScriptForExecuteOnRouter(IUnitOfWork ufw, string scriptName, string userName) {
            var scripts = Get(ufw, sc => sc.Name == scriptName);
            var userRep = ufw.GetRepository<User>();
            var user = userRep.Get(u => u.Nik == userName || u.Phone != null && u.Phone.Contains(userName)).FirstOrDefault();
            if(user == null) {
                throw new NotAuthorizedException();
            }
            var script = scripts.FirstOrDefault(sc => sc.ExecuteScriptUsers.FirstOrDefault(u => u.UserId == user.Id) != null);
            if(script is null) {
                return null;
            }
            var scriptResult = MapModelFromDbToModelToView.MapScriptFromDbToModel(script);
            return scriptResult;
        }
    }
}