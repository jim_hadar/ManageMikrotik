﻿using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.BLL.Services.Db.Concrete {
    public class UserService : BaseObjectService<User>, IUserService<User> {

        public User GetUserByNik(IUnitOfWork ufw, string username) {
            var result = Get(ufw, user => user.Nik == username).FirstOrDefault();
            return result;
        }
    }
}
