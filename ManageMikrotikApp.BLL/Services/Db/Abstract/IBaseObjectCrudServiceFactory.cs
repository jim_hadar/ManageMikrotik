﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IBaseObjectCrudServiceFactory {
        IBaseObjectCrudService<TEntity> Create<TEntity>() where TEntity : IBaseObject;
    }
}
