﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IServiceManager : IService {
        IBaseObjectCrudService<TEntity> GetService<TEntity>() where TEntity : IBaseObject;

        IService GetNoDbService<TService>() where TService : IService;

        void RegisterService<TEntity>(IBaseObjectCrudService<TEntity> service) where TEntity : IBaseObject;

        void RegisterService<TEntity>() where TEntity : IBaseObject;

        void RegisterNoDbService<TService>() where TService : IService, new();

        void RegisterNoDbService<TService>(TService service) where TService : IService;
    }
}
