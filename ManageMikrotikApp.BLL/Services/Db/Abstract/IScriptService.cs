﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IScriptService : IBaseObjectService<Script>{
        /// <summary>
        /// return script model for execute on routers
        /// </summary>
        /// <param name="ufw"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        Abstractions.Models.Script GetScriptForExecuteOnRouter(IUnitOfWork ufw, Func<Script, bool> predicate);
        /// <summary>
        /// return script model for execute on routers
        /// </summary>
        /// <param name="ufw"></param>
        /// <param name="scriptName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        Abstractions.Models.Script GetScriptForExecuteOnRouter(IUnitOfWork ufw, string scriptName, string userName);
    }
}
