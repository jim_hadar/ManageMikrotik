﻿namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IServiceManagerFactory {
        IServiceManager Create();
    }
}
