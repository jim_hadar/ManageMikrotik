﻿using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IBaseObjectService<TEntity> : IBaseObjectCrudService<TEntity>
        where TEntity: IBaseObject{
        PaginateResultModel<TEntity> GetDataForGridAsync(IUnitOfWork ufw, PaginateModel paginateModel, string loadUrl);
    }
}
