﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IBaseObjectCrudService<TEntity> : IService<TEntity> 
        where TEntity : IBaseObject{
        /// <summary>
        /// delete entities by predicate
        /// </summary>
        /// <param name="predicate"></param>
        void Delete(IUnitOfWork ufw, Func<TEntity, bool> predicate);
        /// <summary>
        /// get entities by predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<TEntity> Get(IUnitOfWork ufw, Func<TEntity, bool> predicate);
        /// <summary>
        /// get all entities
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll(IUnitOfWork ufw);
        /// <summary>
        /// get entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity Get(IUnitOfWork ufw, int id);
        /// <summary>
        /// add new entity to database
        /// </summary>
        /// <param name="entity">entity to append</param>
        /// <returns></returns>
        TEntity Append(IUnitOfWork ufw, TEntity entity);
        /// <summary>
        /// update entity
        /// </summary>
        /// <param name="entity">entity to update</param>
        /// <returns></returns>
        TEntity Update(IUnitOfWork ufw, TEntity entity);
    }
}
