﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface IUserService<TEntity> : IBaseObjectService<TEntity>
        where TEntity : User {
        TEntity GetUserByNik(IUnitOfWork ufw, string username);
    }
}
