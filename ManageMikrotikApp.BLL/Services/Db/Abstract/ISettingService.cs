﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {
    public interface ISettingService : IBaseObjectCrudService<Settings>  {
        string GetTelegramBotAuthentificationToken(IUnitOfWork ufw);
        string GetTelegramCommonChannelId(IUnitOfWork ufw);
    }
}
