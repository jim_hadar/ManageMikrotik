﻿using System;

namespace ManageMikrotikApp.BLL.Services.Db.Abstract {

    public interface IService<T> : IService {

    }

    public interface IService : IDisposable {
    }
}
