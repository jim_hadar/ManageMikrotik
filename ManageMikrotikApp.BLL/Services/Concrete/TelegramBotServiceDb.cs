﻿using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.BLL.Helpers.Parser;
using ManageMikrotikApp.BLL.Services.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.Common.Constant;
using ManageMikrotikApp.Common.Exceptions;
using ManageMikrotikApp.Common.Services.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

//http://aftamat4ik.ru/pishem-bota-telegram-na-c/
namespace ManageMikrotikApp.BLL.Services.Concrete {
    public class TelegramBotServiceDb : ManageMikrotikServiceDb, ITelegramBotManageService {
        #region variables
        private TelegramBotClient _bot;

        private string commonChannelId = null;
        #endregion

        public TelegramBotServiceDb(ISshServiceFactory sshServiceFactory,
                                    IUnitOfWorkFactory unitOfWorkFactory,
                                    IServiceManager serviceManager)
            : base(sshServiceFactory, unitOfWorkFactory, serviceManager) {
            
        }

        public async override void SendResponseMessage(object sender, EventArgs e) {
            var messageEventArgs = (MessageEventArgs)e;
            var curUser = messageEventArgs.Message.Chat.Username;
            var message = messageEventArgs.Message;

            try {
                using(var ufw = _unitOfWorkFactory.Create()) {
                    if (string.IsNullOrEmpty(this.commonChannelId)) {
                        lock (_bot) {
                            this.commonChannelId = settingsService.GetTelegramCommonChannelId(ufw);
                        }
                    }

                    var user = userService.GetUserByNik(ufw, curUser);

                    if(user is null) {
                        throw new NotAuthorizedException();
                    }

                    if (message.Text.Contains(Constants.AccessRightToCommane)) {
                        await SetRightToScriptForUser(ufw, message, user);
                        return;
                    }

                    switch (message.Text) {
                        case Constants.HelpCommand:
                            await SendHelpMessageAsync(ufw, message.Chat, curUser);
                            break;
                        default:
                            SendResponseMessage(ufw, message, curUser);
                            break;
                    }
                }                
            }
            catch(Exception exOuter) {
                await _bot.SendTextMessageAsync(message.Chat.Id, exOuter.Message);
                Console.WriteLine(exOuter.Message);
            }            
        }

        private async void SendResponseMessage(IUnitOfWork ufw, Message message, string curUser) {
            try {
                string resultMessage;

                if(!CheckUserRightOnScript(ufw, curUser, message.Text)) {
                    await _bot.SendTextMessageAsync(message.Chat.Id, "Access denied", ParseMode.Html);
                    return;
                }

                await _bot.SendTextMessageAsync(message.Chat, $"Начато выполнение команды <b>{message.Text}</b>...", ParseMode.Html, replyMarkup: CreateInlineKeyboardButton(ufw, curUser));

                resultMessage = await ExecuteCommand(message.Text, true, curUser).ConfigureAwait(false);

                Console.WriteLine($"Результат выполнения команды: {resultMessage}");

                await _bot.SendTextMessageAsync(message.Chat.Id, $"{resultMessage}", ParseMode.Html);

                var messageForChannel = $"Пользователь <b>{curUser}</b> выполнил команду <b>{message.Text}</b>.";
                SendTextMessageToCommonChannel(messageForChannel);
            }
            catch (Exception ex) {
                await _bot.SendTextMessageAsync(message.Chat.Id, ex.Message, ParseMode.Html);
                var messageForChannel = $"При выполнении команды <b>{message.Text}</b> пользователем <b>{curUser}</b> произошла ошибка:\n" +
                    $"{ex.Message}";
                SendTextMessageToCommonChannel(messageForChannel);
                Console.WriteLine($"При обработке команды произошла ошибка: {ex.Message}");
            }
            finally {
                Console.WriteLine("----------------------------------------------------------\n");
            }
        }

        /// <summary>
        /// send message to common channel
        /// </summary>
        /// <param name="message"></param>
        private async void SendTextMessageToCommonChannel(string message) {
            if (!string.IsNullOrEmpty(this.commonChannelId)) {
                try {
                    await _bot.SendTextMessageAsync(commonChannelId, message, ParseMode.Html);
                }
                catch(Exception e) {
                    Console.WriteLine("ошибка при отправке сообщения в общий канал");
                }
            }
        }

        public override void Start() {
            Console.WriteLine("Telegram bot for many hosts is start...");
            string token;
            using (IUnitOfWork ufw = _unitOfWorkFactory.Create()) {
                token = settingsService.GetTelegramBotAuthentificationToken(ufw);
            }
            _bot = new TelegramBotClient(token);
            _bot.OnMessage += SendResponseMessage;
            _bot.OnMessageEdited += SendResponseMessage;
            _bot.OnCallbackQuery += _bot_OnCallbackQuery;

            Console.WriteLine("_bot is start receiving");

            _bot.StartReceiving();
        }

        private void _bot_OnCallbackQuery(object sender, CallbackQueryEventArgs e) {
            try {
                using (IUnitOfWork ufw = _unitOfWorkFactory.Create()) {
                    var message = e.CallbackQuery.Message;
                    message.Text = e.CallbackQuery.Data;

                    string curUser = e.CallbackQuery.Message.Chat.Username;

                    SendResponseMessage(ufw, message, curUser);
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public override void Stop() {
            _bot?.StopReceiving();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    base.Dispose(disposing);
                    Stop();
                }
            }
        }        

        #region additional methods
        private async Task SendHelpMessageAsync(IUnitOfWork ufw, Chat chat, string username) {
            List<InlineKeyboardButton[]> inlineKeyboardButtons = new List<InlineKeyboardButton[]>();

            var scripts = GetHelp(ufw, username);

            foreach(var script in scripts) {
                var inlineBtn = InlineKeyboardButton.WithCallbackData($"Выполнить {script.Name}", script.Name);
                inlineKeyboardButtons.Add(new InlineKeyboardButton[] { inlineBtn });
            }

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"{Constants.HelpCommand} - краткая информация о всех доступных командах");
            if (userService.GetUserByNik(ufw, username).IsAdmin) {
                stringBuilder.AppendLine($"{Constants.AccessRightToCommane} [username] [command] [allow/deny] - разрешить ([allow]) или запретить ([deny]) доступ к команде [command] пользователю с ником [username]");
            }
            stringBuilder.AppendLine(base.GetHelp(username));

            var keyBoard = new InlineKeyboardMarkup(inlineKeyboardButtons.ToArray());
            await _bot.SendTextMessageAsync(chat, stringBuilder.ToString(), ParseMode.Html, replyMarkup: keyBoard);
        }

        private async Task SetRightToScriptForUser(IUnitOfWork ufw, Message message, DAL.Abstrasctions.Models.User user) {
            if (!user.IsAdmin) {
                await _bot.SendTextMessageAsync(message.Chat, "Недостаточно прав", ParseMode.Default);
                return;
            }
            (string username, string command, string right) = this.ParseAndSaveAllowRightOnCommand(ufw, _serviceManager, message.Text);
            await _bot.SendTextMessageAsync(message.Chat, "Права успешно установлены", ParseMode.Default);

            SendTextMessageToCommonChannel($"Пользователь <b>{user.Nik}</b> установил права [{right}] пользователю {username} на скрипт {command}");
        }
        public IReplyMarkup CreateInlineKeyboardButton(IUnitOfWork ufw, string curUser) {
            DAL.Abstrasctions.Models.User user = userService.Get(ufw, u => u.Nik == curUser).FirstOrDefault();
            HashSet<KeyboardButton> keyboardButtons = new HashSet<KeyboardButton>();
            var scripts = scriptService.GetAll(ufw).Where(sc => sc.ExecuteScriptUsers.FirstOrDefault(u => u.UserId == user.Id) != null);
            foreach(var script in scripts) {
                keyboardButtons.Add(new KeyboardButton {
                    Text = script.Name
                });
            }
            return new ReplyKeyboardMarkup(keyboardButtons, resizeKeyboard: true);
        }
        #endregion
    }
}