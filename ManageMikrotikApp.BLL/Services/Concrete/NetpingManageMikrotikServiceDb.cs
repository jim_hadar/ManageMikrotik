﻿using Lextm.SharpSnmpLib.Messaging;
using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.BLL.Models;
using ManageMikrotikApp.BLL.Services.Abstract;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.Common.Exceptions;
using ManageMikrotikApp.Common.Helpers;
using ManageMikrotikApp.Common.Services.Abstract;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Linq;
using ManageMikrotikApp.DAL.Abstrasctions.Enums;
using System.Threading;

namespace ManageMikrotikApp.BLL.Services.Concrete {
    public class NetpingManageMikrotikServiceDb : ManageMikrotikServiceDb, INetPingManageService {
        #region variables
        public HttpClient HttpClient => _httpClient;
        public Listener Listener { get; private set; }
        public NetPingConfiguration Config { get; private set; }
        private object lockObj = new object();
        #endregion
        public NetpingManageMikrotikServiceDb(ISshServiceFactory sshServiceFactory, 
                                              IUnitOfWorkFactory unitOfWorkFactory, 
                                              IServiceManager serviceManager) 
            : base(sshServiceFactory, unitOfWorkFactory, serviceManager) {
            Listener = new Listener();
            Listener.AddBinding(new IPEndPoint(IPAddress.Any, 162));
            Listener.MessageReceived += SendResponseMessage;

            SetHttpClientAuthHeaders();
        }

        public async override void SendResponseMessage(object sender, EventArgs e) {
            var eventArg = (MessageReceivedEventArgs)e;
            try {
                Console.WriteLine($"---Message receive: {eventArg.Message.Pdu().ToString()}---");
                string ipAddress = this.GetIpAddressFromArguments(eventArg);

                (string phoneNumber, string cmd) = this.GetDataFromArguments(eventArg);
                try {
                    Console.WriteLine($"IpAddress:{ipAddress}, Phone: {phoneNumber}, Script: {cmd}");

                    if(!CheckUserRightOnScript(ipAddress, phoneNumber, cmd)) {
                        Console.WriteLine("unauth");
                        throw new NotAuthorizedException();
                    }

                    string resultMsg = await ExecuteCommand(cmd, username: phoneNumber).ConfigureAwait(false);

                    Console.WriteLine("resultMessage ok");

                    SetHttpClientAuthHeaders();

                    await this.SendSmsToNetPingAsync(ipAddress, phoneNumber, resultMsg);
                }
                catch (Exception exInner) {
                    Console.WriteLine($"при обработке команды от netping произошла ошибка: {exInner.Message}");
                    await this.SendSmsToNetPingAsync(ipAddress, phoneNumber, exInner.Message);
                }
            }
            catch(Exception exOuter) {
                Console.WriteLine($"Error ListenerOnMessageReceived: {exOuter.Message}");
            }
        }

        public override void Start() {
            Listener.Start();
            Console.WriteLine("NetPIng service is start");
        }

        public override void Stop() {
            Listener.Stop();
        }

        private bool CheckUserRightOnScript(string ipAddress,
                                                string phone,
                                                string scriptName) {
            using(IUnitOfWork ufw = base._unitOfWorkFactory.Create()) {
                var user = userService.Get(ufw, u => u.Phone != null && u.Phone.Contains(phone)).FirstOrDefault();
                if (user is null) {
                    return false;
                }
                Console.WriteLine("user ok");
                return base.CheckUserRightOnScript(ufw, user.Nik, scriptName);
            }
        }


        private void SetHttpClientAuthHeaders() {
            try {
                if (string.IsNullOrEmpty(Config?.UserName) ||
                   string.IsNullOrEmpty(Config?.Password)) {
                    Monitor.Enter(lockObj);
                    try {
                        using (IUnitOfWork ufw = _unitOfWorkFactory.Create()) {
                            Config = new NetPingConfiguration() {
                                UserName = settingsService.Get(ufw, (int)SettingsEnum.NetPinUserName).Value,
                                Password = settingsService.Get(ufw, (int)SettingsEnum.NetPingPassword).Value
                            };
                        }
                        var byteArray = new UTF8Encoding().GetBytes($"{Config.UserName}:{Config.Password}");
                        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    }
                    finally {
                        Monitor.Exit(lockObj);
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
