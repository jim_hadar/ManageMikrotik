﻿using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.BLL.Services.Db.Abstract;
using ManageMikrotikApp.Common;
using ManageMikrotikApp.Common.Exceptions;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.UnitOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.BLL.Services.Abstract {
    public abstract class ManageMikrotikServiceDb : BaseManageMikrotikService {
        #region variables
        protected IUnitOfWorkFactory _unitOfWorkFactory;

        protected IServiceManager _serviceManager;

        protected IScriptService scriptService => (IScriptService)_serviceManager.GetService<DAL.Abstrasctions.Models.Script>();

        protected IUserService<User> userService => (IUserService<User>)_serviceManager.GetService<User>();

        protected ISettingService settingsService => (ISettingService)_serviceManager.GetService<Settings>();

        #endregion
        public ManageMikrotikServiceDb(ISshServiceFactory sshServiceFactory,
                                       IUnitOfWorkFactory unitOfWorkFactory,
                                       IServiceManager serviceManager) 
            : base(sshServiceFactory) {
            _unitOfWorkFactory = unitOfWorkFactory;
            _serviceManager = serviceManager;
        }

        protected virtual List<DAL.Abstrasctions.Models.Script> GetHelp(IUnitOfWork ufw, string username) {
            var user = userService.Get(ufw, u => u.Nik == username || u.Phone != null && u.Phone.Contains(username)).FirstOrDefault();
            if (user is null) {
                throw new NotAuthorizedException();
            }
            var builder = new StringBuilder();
            if (user.IsAdmin) {
                return scriptService.GetAll(ufw).ToList();
            }
            if (user.ExecuteScriptUsers?.ToList()?.Count == 0) {
                throw new ScriptForUserNotFoundException($"Не найдено скриптов для пользователя {username}");
            }
            else {
                var result = new List<DAL.Abstrasctions.Models.Script>();
                user.ExecuteScriptUsers.ToList().ForEach(scriptUserToExecute => {
                    if (scriptUserToExecute.IsAllow.Value) {
                        var script = scriptUserToExecute.Script;
                        result.Add(script);
                    }
                });
                return result;
            }
        }

        public override string GetHelp(string username = "") {
            using(IUnitOfWork ufw = _unitOfWorkFactory.Create()) {
                var scripts = GetHelp(ufw, username);
                var builder = new StringBuilder();
                scripts.ForEach(script => {
                    builder.AppendLine(GetHelpOneScript(script.Name, script.Description));
                });
                return builder.ToString();
            }
        }

        public override Abstractions.Models.Script FindScript(string commandName, string username = "") {
            using (IUnitOfWork ufw = _unitOfWorkFactory.Create()) {
                var scriptToExecute = scriptService.GetScriptForExecuteOnRouter(ufw, 
                                                        commandName, username);
                return scriptToExecute;
            }
        }

        protected virtual bool CheckUserRightOnScript(IUnitOfWork ufw, string username, string scriptName) {
            var user = userService.GetUserByNik(ufw, username);
            var isAdmin = user?.IsAdmin;
            if (isAdmin.HasValue && isAdmin.Value)
                return true;
            if(user?.ExecuteScriptUsers != null &&
                user.ExecuteScriptUsers.Count != 0) {
                var script = user.ExecuteScriptUsers.FirstOrDefault(
                                                            executeScriptUser =>
                                                                            executeScriptUser.Script.Name == scriptName &&
                                                                            executeScriptUser.IsAllow.Value);
                return script != null;                
            }
            return false;
        }
    }
}