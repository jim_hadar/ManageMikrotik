﻿using System;

namespace ManageMikrotikApp.Common.Exceptions {
    public class InCorrectCommandException : Exception {
        public InCorrectCommandException()
            : base("Script not found") {
        }

        public InCorrectCommandException(string message)
            : base(message) {
        }

        public InCorrectCommandException(string message, Exception innerException)
            : base(message, innerException) {
        }
    }
}