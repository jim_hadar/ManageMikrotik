﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.Common.Exceptions {
    public class ScriptForUserNotFoundException : Exception {
        public ScriptForUserNotFoundException() 
            : base("For current user scripts not found") {
        }

        public ScriptForUserNotFoundException(string message)
            : base(message) {

        }
    }
}
