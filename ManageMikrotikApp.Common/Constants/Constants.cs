﻿
namespace ManageMikrotikApp.Common.Constant {
    public class Constants {
        public const string HelpCommand = "/help";

        public const string AccessRightToCommane = "/accessToCommand";
    }
}
