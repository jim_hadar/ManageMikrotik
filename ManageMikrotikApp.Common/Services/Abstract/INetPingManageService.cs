﻿using System.Net.Http;

namespace ManageMikrotikApp.Common.Services.Abstract {
    public interface INetPingManageService {
        HttpClient HttpClient { get; }
    }
}
