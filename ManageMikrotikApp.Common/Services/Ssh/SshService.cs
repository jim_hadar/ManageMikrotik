﻿using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.Abstractions.Ssh;
using Renci.SshNet;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Common.Ssh {
    public class SshService : SshClient, ISshService {
        public readonly SshConnectionInfo _sshConnectionInfo;
        public SshService(SshConnectionInfo sshConnectionInfo)
            :base(sshConnectionInfo.Host ?? string.Empty, 
                  sshConnectionInfo.Port, 
                  sshConnectionInfo.UserName ?? string.Empty, 
                  sshConnectionInfo.Password ?? string.Empty){
            _sshConnectionInfo = sshConnectionInfo;
        }

        public SshConnectionInfo SshConnectionInfo => _sshConnectionInfo;

        public string ExecuteCommand(in string commandToExecute) {
            if (!base.IsConnected) {
                base.Connect();
            }
            var sshCommand = CreateCommand(commandToExecute);
            return sshCommand.Execute();
        }

        private bool _isDisposed;

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (!_isDisposed) {
                    base.Dispose(disposing);
                    this._isDisposed = true;
                }
            }
        }
    }
}
