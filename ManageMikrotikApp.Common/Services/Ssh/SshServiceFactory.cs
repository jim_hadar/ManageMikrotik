﻿using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.Abstractions.Ssh;

namespace ManageMikrotikApp.Common.Ssh {
    public class SshServiceFactory : ISshServiceFactory {
        public ISshService CreateService(SshConnectionInfo sshConnectionInfo) {
            return new SshService(sshConnectionInfo);
        }
    }
}
