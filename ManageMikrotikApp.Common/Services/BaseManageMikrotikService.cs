﻿using ManageMikrotikApp.Abstractions;
using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.Abstractions.Ssh;
using System;
using System.Text;
using System.Threading.Tasks;
using ManageMikrotikApp.Common.Constant;
using System.Net.Http;
using ManageMikrotikApp.Common.Exceptions;

namespace ManageMikrotikApp.Common {
    public abstract class BaseManageMikrotikService : IManageMikrotikService {

        protected HttpClient _httpClient;

        protected ISshServiceFactory _sshServiceFactory;

        public BaseManageMikrotikService(ISshServiceFactory sshServiceFactory) {
            _sshServiceFactory = sshServiceFactory;
            _httpClient = new HttpClient();
        }
        /// <summary>
        /// execute command on router or server
        /// </summary>
        /// <param name="scriptName">script to execute</param>
        /// <param name="isNeedDetailedResult"></param>
        /// <param name="username">curUserName</param>
        /// <returns></returns>
        public virtual async Task<string> ExecuteCommand(string scriptName, bool isNeedDetailedResult = false, string username = "") {
            string resultMessage = string.Empty;
            switch (scriptName.Trim()) {
                case Constants.HelpCommand:
                    resultMessage = GetHelp(username);
                    break;
                default:
                    var script = FindScript(scriptName, username); 
                    if(script is null) {
                        throw new InCorrectCommandException();
                    }
                    resultMessage = await ExecuteCommandOnMikrotikAsync(_sshServiceFactory, script, isNeedDetailedResult);
                    break;
            }
            return resultMessage;
        }

        /// <summary>
        /// execute command sync
        /// </summary>
        /// <param name="sshServiceFactory"></param>
        /// <param name="command"></param>
        /// <param name="isNeedDetailedResult"></param>
        /// <returns></returns>
        protected virtual string ExecuteCommandOnMikrotik(ISshServiceFactory sshServiceFactory, Script command, bool isNeedDetailedResult) {
            var resultMsg = new StringBuilder();
            Console.WriteLine($"---Начато выполнение команды {command.ScriptName}---");

            foreach (var router in command.Routers) {
                Console.WriteLine($"---Пытаемся подключиться к роутеру {router.RouterName} с адресом {router.SshConnectionInfo.Host}:{router.SshConnectionInfo.Port}---");
                try {
                    using (ISshService sshClient = sshServiceFactory.CreateService(router.SshConnectionInfo)) {
                        sshClient.Connect();

                        var connectInfo = sshClient.SshConnectionInfo;

                        resultMsg.AppendLine($"<b>Commands on host {connectInfo.Host}:{connectInfo.Port}</b>");

                        foreach (var c in router.CommandsToExecute) {
                            string resultExecuteCommand = sshClient.ExecuteCommand(c);
                            string msg;
                            if (string.IsNullOrEmpty(resultExecuteCommand) || !isNeedDetailedResult) {
                                msg = $"Script {c} is success completed";
                            }
                            else {
                                msg = $"Result Execute command <b>{c}</b>:\n{resultExecuteCommand}";
                            }
                            resultMsg.AppendLine(msg);
                            Console.WriteLine(msg);
                        }

                        sshClient.Disconnect();
                    }
                }
                catch (Exception e) {
                    Console.WriteLine($"Error: {e.Message}");
                    resultMsg.AppendLine($"<b>Could not connect to host or execute command on router {router.RouterName} {router.SshConnectionInfo.Host}:{router.SshConnectionInfo.Port}</b>");
                }
            }

            Console.WriteLine($"---Выполнение команды {command.ScriptName} завершено---");
            return resultMsg.ToString();
        }
        /// <summary>
        /// execute command async
        /// </summary>
        /// <param name="sshServiceFactory"></param>
        /// <param name="command"></param>
        /// <param name="isNeedDetailedResult"></param>
        /// <returns></returns>
        protected virtual Task<string> ExecuteCommandOnMikrotikAsync(ISshServiceFactory sshServiceFactory, Script command, bool isNeedDetailedResult) {
            var task = Task.Run(() => {
                return ExecuteCommandOnMikrotik(sshServiceFactory, command, isNeedDetailedResult);
            });
            return task;
        }
        /// <summary>
        /// return standard line with script name and description
        /// </summary>
        /// <param name="scriptName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        protected virtual string GetHelpOneScript(string scriptName, string description) {
            return string.Format(" - <b>{0}</b> - {1}", scriptName, description);
        }

        public abstract void Start();

        public abstract void Stop();

        public abstract void SendResponseMessage(object sender, EventArgs e);

        protected bool _disposed = false;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    Stop();
                    _httpClient.Dispose();
                    _disposed = true;
                }
            }
        }

        public virtual Script FindScript(string commandName, string username = "") {
            throw new NotImplementedException();
        }

        public virtual string GetHelp(string username = "") {
            throw new NotImplementedException();
        }
    }
}