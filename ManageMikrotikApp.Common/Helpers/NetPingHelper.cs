﻿using Lextm.SharpSnmpLib.Messaging;
using ManageMikrotikApp.Common.Services.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Common.Helpers {
    public static class NetPingHelper {
        /// <summary>
        /// разбинение строки на массив, в котором каждая строка содержит по 70 элементов
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static List<string> SplitString(this INetPingManageService netPingManageService, string str) {
            str = str.Replace("<b>", string.Empty).Replace("</b>", string.Empty);
            List<string> list = new List<string>();
            int i = 0;
            while (i < str.Length - 1) {
                if (str.Length - 1 - i < 70) {
                    list.Add(str.Substring(i));

                }
                else {
                    list.Add(str.Substring(i, 70));
                }
                i += 70;
            }

            var msgLast = list.Last();
            if (msgLast.Length < 70) {
                msgLast +=
                    "                                                                                                                         ";
            }

            list[list.Count - 1] = msgLast;
            return list;
        }

        public static async Task SendSmsToNetPingAsync(this INetPingManageService netPingManageService, string ipAddress, string phone, string message) {
            try {
                List<string> messages = netPingManageService.SplitString(message);
                Uri uri = new Uri($"http://{ipAddress}/sendsms.cgi?utf8");

                foreach (var msg in messages) {
                    HttpContent content = new StringContent($"[{phone}] {msg}");
                    var response = await netPingManageService.HttpClient.PostAsync(uri, content);

                    var contentResponseStream = await response.Content.ReadAsStreamAsync();
                    using (StreamReader streamReader = new StreamReader(contentResponseStream)) {
                        var contentResponse = streamReader.ReadToEndAsync();
                        Console.WriteLine($"Результат отправки сообщения '{msg}': \nStatusCode: {response.StatusCode}. ResponseString: {contentResponse}");
                    }
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// get ip address from arguments from netping snmp
        /// </summary>
        /// <param name="netPingManageService"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string GetIpAddressFromArguments(this INetPingManageService netPingManageService, MessageReceivedEventArgs e) {
            return e.Sender.Address.ToString();
        }
        /// <summary>
        /// get phone number and command for execute on router from arguments from netping snmp
        /// </summary>
        /// <param name="netPingManageService"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static (string phone, string command) GetDataFromArguments(this INetPingManageService netPingManageService, MessageReceivedEventArgs e) {
            var data = e.Message.Scope.Pdu.Variables;
            string phoneNumber = data[1].Data.ToString();
            if (phoneNumber.StartsWith('7')) {
                phoneNumber = "+" + phoneNumber;
            }
            string cmd = data[0].Data.ToString();
            return (phone: phoneNumber, command: cmd);
        }
    }
}