﻿using System.Collections.Generic;

namespace ManageMikrotikApp.Abstractions.Models {
    public class Script {
        public Script() {
            this.Routers = new List<Router>();
        }
        public string ScriptName { get; set; }

        public string Description { get; set; }

        public List<Router> Routers { get; set; }
    }
}