﻿using Newtonsoft.Json;

namespace ManageMikrotikApp.Abstractions.Models {
    public class SshConnectionInfo : Authentification {
        [JsonProperty("host")]
        public string Host { get; set; }
        [JsonProperty("port")]
        public int Port { get; set; }
        public string PublicSshKey { get; set; }
    }
}
