﻿using System.Collections.Generic;

namespace ManageMikrotikApp.Abstractions.Models {
    public class Router {
        public Router() {
            SshConnectionInfo = new SshConnectionInfo();
        }
        public string RouterName { get; set; }

        public SshConnectionInfo SshConnectionInfo { get; set; }

        public List<string> CommandsToExecute { get; set; }
    }
}
