﻿using Newtonsoft.Json;

namespace ManageMikrotikApp.Abstractions.Models {
    public class Authentification {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
