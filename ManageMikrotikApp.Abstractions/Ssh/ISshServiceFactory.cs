﻿using ManageMikrotikApp.Abstractions.Models;

namespace ManageMikrotikApp.Abstractions.Ssh {
    public interface ISshServiceFactory {
        /// <summary>
        /// создание сервиса для установления ssh соединения
        /// </summary>
        /// <returns></returns>
        ISshService CreateService(SshConnectionInfo sshConnectionInfo);
    }
}
