﻿using ManageMikrotikApp.Abstractions.Models;
using System;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Abstractions.Ssh {
    public interface ISshService : IDisposable {

        SshConnectionInfo SshConnectionInfo { get; }

        /// <summary>
        /// установка соединения по ssh
        /// </summary>
        void Connect();
        /// <summary>
        /// закрытие соединения по ssh
        /// </summary>
        void Disconnect();

        /// <summary>
        /// execute command in ssh server
        /// </summary>
        /// <param name="commandToExecute"></param>
        /// <returns></returns>
        string ExecuteCommand(in string commandToExecute);
    }
}
