﻿using ManageMikrotikApp.Abstractions.Ssh;
using System;
using System.Threading.Tasks;

namespace ManageMikrotikApp.Abstractions {
    /// <summary>
    /// управление маршрутизатором
    /// </summary>
    public interface IManageMikrotikService : IDisposable {
        /// <summary>
        /// начало работы сервиса
        /// </summary>
        void Start();
        /// <summary>
        /// окончание работы сервиса
        /// </summary>
        void Stop();
        /// <summary>
        /// execute command on router or server
        /// </summary>
        /// <param name="commandName">script to execute</param>
        /// <param name="isNeedDetailedResult"></param>
        /// <param name="username">curUserName</param>
        /// <returns></returns>
        Task<string> ExecuteCommand(string commandName, bool isNeedDetailedResult = false, string username = "");
        /// <summary>
        /// обработка отсылки ответного сообщения на соответствующее устройство
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendResponseMessage(object sender, EventArgs e);
        /// <summary>
        /// find scripts for user
        /// </summary>
        /// <param name="commandName"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        Abstractions.Models.Script FindScript(string commandName, string username = "");
        /// <summary>
        /// return script for current user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        string GetHelp(string username = "");
    }
}