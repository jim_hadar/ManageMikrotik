﻿using ManageMikrotikApp.Abstractions.Models;
using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.Common;
using ManageMikrotikApp.Common.Constant;
using ManageMikrotikApp.Common.Exceptions;
using ManageMikrotikApp.NoDb.Models;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageMikrotikApp.NoDb.Services.Abstract {
    public abstract class ManageMikrotikServiceNoDb : BaseManageMikrotikService {

        protected Config _baseConfig;

        public ManageMikrotikServiceNoDb(ISshServiceFactory sshServiceFactory, 
                                         Config config = null) 
            : base(sshServiceFactory) {
            _baseConfig = config;
        }

        public override string GetHelp(string username = "") {
            var scripts = _baseConfig.Scripts;
            var stringBuilder = new StringBuilder();
            scripts.ForEach(script => { stringBuilder.AppendLine(GetHelpOneScript(script.ScriptName, script.Description)); });
            return stringBuilder.ToString();
        }

        public override Script FindScript(string commandName, string username = "") {
            return _baseConfig.Scripts.FirstOrDefault(script => script.ScriptName == commandName);
        }

        public abstract override void SendResponseMessage(object sender, EventArgs e);

        public abstract override void Start();

        public abstract override void Stop();
    }
}