﻿using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.Common.Services.Abstract;
using ManageMikrotikApp.NoDb.Models;
using ManageMikrotikApp.NoDb.Services.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace ManageMikrotikApp.NoDb.Services.Concrete {
    public sealed class TelegramBotServiceNoDb : ManageMikrotikServiceNoDb, ITelegramBotManageService {
        #region variables
        private TelegramBotClient _bot;

        internal TelegramBotConfig Config => (TelegramBotConfig)_baseConfig;

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sshServiceFactory">ssh service factory</param>
        /// <param name="configFilePath">full path to config file</param>
        public TelegramBotServiceNoDb(ISshServiceFactory sshServiceFactory,
                                      string configFilePath) 
            : base(sshServiceFactory) {
            var directory = System.IO.Path.GetDirectoryName(configFilePath);
            var fileName = System.IO.Path.GetFileName(configFilePath);
            var configuration = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile(fileName, true, true)
                .Build();
            var config = new TelegramBotConfig();
            configuration.Bind(config);
            _baseConfig = config;
        }

        public override async void SendResponseMessage(object sender, EventArgs e) {
            var messageEventArgs = (MessageEventArgs)e;
            var curUser = messageEventArgs.Message.Chat.Username;
            var message = messageEventArgs.Message;
            try {
                string resultMessage;
                if (!Config.AllowUsers.Contains(curUser)) {
                    await _bot.SendTextMessageAsync(message.Chat.Id, "Access denied");
                    return;
                }

                resultMessage = await ExecuteCommand(message.Text, true).ConfigureAwait(false);

                Console.WriteLine($"Результат выполнения команды: {resultMessage}");

                await _bot.SendTextMessageAsync(message.Chat.Id, $"{resultMessage}", ParseMode.Html);

                var messageForChannel = $"Пользователь <b>{curUser}</b> выполнил команду <b>{message.Text}</b>.";
                await _bot.SendTextMessageAsync(Config.CommonChannelId, messageForChannel, ParseMode.Html);
            }
            catch (Exception ex) {
                await _bot.SendTextMessageAsync(message.Chat.Id, ex.Message, ParseMode.Html);
                var messageForChannel = $"При выполнении команды <b>{message.Text}</b> пользователем <b>{curUser}</b> произошла ошибка:\n" +
                    $"{ex.Message}";
                await _bot.SendTextMessageAsync(Config.CommonChannelId, messageForChannel, ParseMode.Html);
                Console.WriteLine($"При обработке команды произошла ошибка: {ex.Message}");
            }
            finally {
                Console.WriteLine("----------------------------------------------------------\n");
            }
        }

        public override void Start() {
            Console.WriteLine("Telegram bot for many hosts is start...");
            var token = this.Config.AuthentificationToken;
            _bot = new TelegramBotClient(token);
            _bot.OnMessage += SendResponseMessage;
            _bot.OnMessageEdited += SendResponseMessage;

            Console.WriteLine("_bot is start receiving");

            _bot.StartReceiving();
        }

        public override void Stop() {
            _bot?.StopReceiving();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    base.Dispose(disposing);
                    Stop();
                }
            }
        }
    }
}