﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using ManageMikrotikApp.Abstractions.Ssh;
using ManageMikrotikApp.NoDb.Models;
using ManageMikrotikApp.NoDb.Services.Abstract;
using Microsoft.Extensions.Configuration;
using Lextm.SharpSnmpLib.Messaging;
using System.Net;
using ManageMikrotikApp.Common.Services.Abstract;
using ManageMikrotikApp.Common.Helpers;
using ManageMikrotikApp.Common.Exceptions;
using System.Net.Http.Headers;

namespace ManageMikrotikApp.NoDb.Services.Concrete {
    public sealed class NetpingManageMikrotikServiceNoDb : ManageMikrotikServiceNoDb, INetPingManageService {
        #region variables
        public Listener Listener { get; private set; }

        public NetPingConfig Config => (NetPingConfig)_baseConfig;

        public HttpClient HttpClient { get => _httpClient; }
        #endregion

        public NetpingManageMikrotikServiceNoDb(ISshServiceFactory sshServiceFactory,
                                                string configFilePath) 
            : base(sshServiceFactory) {
            var directory = System.IO.Path.GetDirectoryName(configFilePath);
            var fileName = System.IO.Path.GetFileName(configFilePath);
            var configuration = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile(fileName, true, true)
                .Build();
            var config = new NetPingConfig();
            configuration.Bind(config);
            _baseConfig = config;

            var byteArray = new UTF8Encoding().GetBytes($"{Config.NetPing.Authentificate.UserName}:{Config.NetPing.Authentificate.Password}");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            this.Listener = new Listener();
            Listener.AddBinding(new IPEndPoint(IPAddress.Any, 162));
            Listener.MessageReceived += SendResponseMessage;
        }
        public override async void SendResponseMessage(object sender, EventArgs e) {
            var eventArg = (MessageReceivedEventArgs)e;
            try {
                Console.WriteLine($"---Message receive: {eventArg.Message.Pdu().ToString()}---");
                string ipAddress = this.GetIpAddressFromArguments(eventArg);

                (string phoneNumber, string cmd) = this.GetDataFromArguments(eventArg);

                try {
                    Console.WriteLine($"IpAddress:{ipAddress}, Phone: {phoneNumber}, Script: {cmd}");

                    if (!Config.NetPing.AllowIpAddresses.Contains(ipAddress) ||
                        !Config.NetPing.AllowPhoneNumbers.Contains(phoneNumber)) {
                        throw new NotAuthorizedException();
                    }

                    string resultMsg = await ExecuteCommand(cmd).ConfigureAwait(false);

                    await this.SendSmsToNetPingAsync(ipAddress, phoneNumber, resultMsg);
                }
                catch(Exception exInner) {
                    Console.WriteLine($"при обработке команды от netping произошла ошибка: {exInner.Message}");
                    await this.SendSmsToNetPingAsync(ipAddress, phoneNumber, exInner.Message);
                }
            }
            catch(Exception exOuter) {
                Console.WriteLine($"Error ListenerOnMessageReceived: {exOuter.Message}");
            }
        }

        public override void Start() {
            Listener.Start();
            Console.WriteLine("NetPIng service is start");
        }

        public override void Stop() {
            Listener.Stop();
        }
    }
}