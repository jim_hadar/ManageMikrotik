﻿namespace ManageMikrotikApp.NoDb.Models {
    public class NetPingConfig : Config {
        public NetPingConfig()
            : base() {
            NetPing = new AdditionalNetPingConfig();
        }
        public AdditionalNetPingConfig NetPing { get; set; }
    }
}
