﻿using System.Collections.Generic;

namespace ManageMikrotikApp.NoDb.Models {
    public class TelegramBotConfig : Config {
        public string AuthentificationToken { get; set; }
        /// <summary>
        /// id общего канала. Туда бот рассылает общие сообщения о произошедших действиях
        /// </summary>
        public long CommonChannelId { get; set; }
        public List<string> AllowUsers { get; set; }
    }
}
