﻿using ManageMikrotikApp.Abstractions.Models;
using System.Collections.Generic;

namespace ManageMikrotikApp.NoDb.Models {
    public class AdditionalNetPingConfig {

        public List<string> AllowPhoneNumbers { get; set; }

        public List<string> AllowIpAddresses { get; set; }

        public Authentification Authentificate { get; set; }
    }
}
