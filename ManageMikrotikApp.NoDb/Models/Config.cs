﻿using ManageMikrotikApp.Abstractions.Models;
using System.Collections.Generic;

namespace ManageMikrotikApp.NoDb.Models {
    public class Config {
        public Config() {
            Scripts = new List<Script>();
        }  
        public List<Script> Scripts { get; set; }        
    }
}
