﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public class PostgresRepository<T> : IBaseRepository<T> where T : class, IBaseObject {
        protected DbContext context;
        protected DbSet<T> dbSet;
        public PostgresRepository(DbContext context) {
            this.context = context;
            dbSet = context.Set<T>();
        }

        #region remove methods
        public virtual void Delete(T entity) {
            context.Remove(entity);
        }

        public virtual int Delete(int id) {
            var entityToRemove = dbSet.FirstOrDefault(x => x.Id == id);
            if (entityToRemove != null) {
                dbSet.Remove(entityToRemove);
                return id;
            }
            return 0;
        }

        public void Delete(Func<T, bool> predicate) {
            var entitiesToRemove = dbSet.Where(predicate);
            dbSet.RemoveRange(entitiesToRemove);
        }        
        #endregion

        #region Get methods
        public virtual IEnumerable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate) {
            return GetAll().Where(predicate);
        }

        public virtual T Get(int id, bool hidden = false) {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }        

        public virtual IQueryable<T> GetAll() {
            return dbSet.AsQueryable();
        }

        public IQueryable<T> GetAllWithoutChild() {
            return dbSet.AsQueryable();
        }

        public virtual void Detach(T entity) {
            throw new NotImplementedException();
        }
        #endregion

        public virtual T Hide(int id) {
            throw new NotImplementedException();
        }

        public virtual T Insert(T entity) {
            dbSet.Add(entity);
            return entity;
        }

        public virtual T Update(T entity) {
            dbSet.Update(entity);
            return entity;
        }

        protected bool _disposed = false;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {                    
                    _disposed = true;
                }
            }
        }        
    }
}
