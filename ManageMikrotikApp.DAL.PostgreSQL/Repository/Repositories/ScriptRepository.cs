﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public class ScriptRepository : PostgresRepository<Script> {
        public ScriptRepository(DbContext context) 
            : base(context) {
        }

        public override IQueryable<Script> GetAll() {
            var result = dbSet.Include(x => x.CommandToExecute)
                              .Include(x => x.ScriptType)
                              .Include(x => x.Router)
                                .ThenInclude(router => router.SshConnection)
                              .Include(x => x.Router)
                                .ThenInclude(router => router.CommandToExecute)
                              .Include(x => x.ExecuteScriptUsers)
                                .ThenInclude(executeUsers => executeUsers.User);
            return result;
        }
    }
}
