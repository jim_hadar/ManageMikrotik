﻿using ManageMikrotikApp.DAL.Abstrasctions.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public class UserRepository : PostgresRepository<User> {
        public UserRepository(DbContext context) 
            : base(context) {
        }

        public override IQueryable<User> GetAll() {
            var result = dbSet.Include(x => x.ExecuteScriptUsers)
                                .ThenInclude(executeScript => executeScript.Script);
            return result;
        }
    }
}
