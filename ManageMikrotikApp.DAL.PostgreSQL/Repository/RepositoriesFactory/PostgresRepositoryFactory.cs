﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using Microsoft.EntityFrameworkCore;
using System;

namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public class PostgresRepositoryFactory : IRepositoryFactory {
        protected DbContext _context;
        public PostgresRepositoryFactory(DbContext context) {
            _context = context;
        }
        protected bool _disposed = false;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    _disposed = true;
                }
            }
        }

        public virtual IBaseRepository<T> Create<T>() where T : class, IBaseObject, new() {
            return new PostgresRepository<T>(_context);
        }

        public virtual IBaseRepository Create() {
            throw new NotImplementedException();
        }
    }
}