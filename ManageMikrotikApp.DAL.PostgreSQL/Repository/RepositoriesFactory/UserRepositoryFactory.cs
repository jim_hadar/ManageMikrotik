﻿using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using Microsoft.EntityFrameworkCore;

namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public sealed class UserRepositoryFactory : PostgresRepositoryFactory, IUserRepositoryFactory {
        public UserRepositoryFactory(DbContext context) 
            : base(context) {
        }

        public override IBaseRepository Create() {
            return new UserRepository(_context);
        }
    }
}
