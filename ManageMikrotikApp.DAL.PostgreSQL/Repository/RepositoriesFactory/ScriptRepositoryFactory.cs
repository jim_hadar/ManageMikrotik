﻿using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using Microsoft.EntityFrameworkCore;

namespace ManageMikrotikApp.DAL.PostgreSQL.Repository {
    public class ScriptRepositoryFactory : PostgresRepositoryFactory, IScriptRepositoryFactory {
        public ScriptRepositoryFactory(DbContext context) 
            : base(context) {
        }

        public override IBaseRepository Create() {
            return new ScriptRepository(_context);
        }
    }
}
