﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using ManageMikrotikApp.DAL.Abstrasctions.Repository;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManageMikrotikApp.DAL.PostgreSQL.UnitOfWork {
    public class UnitOfWork : IUnitOfWork {
        protected DbContext _context;

        protected Dictionary<Type, IBaseRepository> _repositories;

        public UnitOfWork(DbContext context) {
            _context = context;
            _repositories = new Dictionary<Type, IBaseRepository>();
        }
        public object DBContext => _context;

        public void Commit() {
            throw new NotImplementedException();
        }

        protected bool _disposed = false;
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (!_disposed) {
                    _context.Database.CloseConnection();                    
                    _disposed = true;
                }
            }
        }

        public IBaseRepository<TEntity> GetRepository<TEntity>() 
            where TEntity : IBaseObject {
            return (IBaseRepository<TEntity>)_repositories[typeof(TEntity)];
        }

        public void RegisterRepository<TEntity>(IBaseRepository repository) 
            where TEntity : IBaseObject {
            if (!_repositories.ContainsKey(typeof(TEntity))) {
                _repositories.Add(typeof(TEntity), repository);
            }
        }

        public void RegisterRepository<TEntity>(IBaseRepository<TEntity> repository)
            where TEntity : IBaseObject {
            this.RegisterRepository<TEntity>((IBaseRepository)repository);
            if (!_repositories.ContainsKey(typeof(TEntity))) {
                _repositories.Add(typeof(TEntity), repository);
            }
        }

        public void Rollback() {
            throw new NotImplementedException();
        }

        public int SaveChanges() {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync() {
            return _context.SaveChangesAsync();
        }
    }
}