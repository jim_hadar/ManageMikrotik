﻿using ManageMikrotikApp.DAL.Abstrasctions.Constants;
using ManageMikrotikApp.DAL.Abstrasctions.Models;
using ManageMikrotikApp.DAL.PostgreSQL.Repository;
using ManageMikrotikApp.UnitOfWork.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.IO;

namespace ManageMikrotikApp.DAL.PostgreSQL.UnitOfWork {
    public class UnitOfWorkFactory : IUnitOfWorkFactory {        

        IConfiguration _configuration;

        public UnitOfWorkFactory(string pathToConfig) {
            var directory = System.IO.Path.GetDirectoryName(pathToConfig);
            var fileName = System.IO.Path.GetFileName(pathToConfig);

            _configuration = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile(fileName, true, true)
                .Build();           
        }

        public UnitOfWorkFactory(IConfiguration configuration) {
            _configuration = configuration;
        }
        public IUnitOfWork Create() {
            var context = new PostrgresDbContext(_configuration[ConfigContants.ConnectionStringName]);
            if (context.Database.GetDbConnection().State != ConnectionState.Open) {
                context.Database.OpenConnection();
            }
            var ufw = new UnitOfWork(context);
            var postgresRepFactory = new PostgresRepositoryFactory(context);
            var userRepFactory = new UserRepositoryFactory(context);
            var scriptRepFactory = new ScriptRepositoryFactory(context);
            //register all repositories
            ufw.RegisterRepository<User>(userRepFactory.Create());
            ufw.RegisterRepository<SshConnection>(postgresRepFactory.Create<SshConnection>());
            ufw.RegisterRepository<Settings>(postgresRepFactory.Create<Settings>());
            ufw.RegisterRepository<ScriptType>(postgresRepFactory.Create<ScriptType>());
            ufw.RegisterRepository<Script>(scriptRepFactory.Create());
            ufw.RegisterRepository<Router>(postgresRepFactory.Create<Router>());
            ufw.RegisterRepository<ExecuteScriptUsers>(postgresRepFactory.Create<ExecuteScriptUsers>());
            ufw.RegisterRepository<CommandToExecute>(postgresRepFactory.Create<CommandToExecute>());
            return ufw;
        }
    }
}