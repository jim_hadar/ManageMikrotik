﻿using Microsoft.EntityFrameworkCore;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class PostrgresDbContext : DbContext
    {
        string connectionString;
        public PostrgresDbContext(string connString)
        {
            connectionString = connString;
        }

        public PostrgresDbContext(DbContextOptions<PostrgresDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CommandToExecute> CommandToExecute { get; set; }
        public virtual DbSet<ExecuteScriptUsers> ExecuteScriptUsers { get; set; }
        public virtual DbSet<Router> Router { get; set; }
        public virtual DbSet<Script> Script { get; set; }
        public virtual DbSet<ScriptType> ScriptType { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<SshConnection> SshConnection { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CommandToExecute>(entity =>
            {
                entity.ToTable("command_to_execute", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.command_to_execute_id_seq'::regclass)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100);

                entity.Property(e => e.RouterId).HasColumnName("router_id");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value");

                entity.HasOne(d => d.Router)
                    .WithMany(p => p.CommandToExecute)
                    .HasForeignKey(d => d.RouterId)
                    .HasConstraintName("command_to_execute_router_fk");
            });

            modelBuilder.Entity<ExecuteScriptUsers>(entity =>
            {
                entity.ToTable("execute_script_users", "common");

                entity.ForNpgsqlHasComment("Связь с пользователя, которым разрешено / запрещено запускать команду");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.execute_command_users_id_seq'::regclass)");

                entity.Property(e => e.IsAllow)
                    .IsRequired()
                    .HasColumnName("is_allow")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.ScriptId).HasColumnName("script_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Script)
                    .WithMany(p => p.ExecuteScriptUsers)
                    .HasForeignKey(d => d.ScriptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("execute_script_users_command_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ExecuteScriptUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("execute_script_users_user_fk");
            });

            modelBuilder.Entity<Router>(entity =>
            {
                entity.ToTable("router", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.router_id_seq'::regclass)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200);

                entity.Property(e => e.ScriptId).HasColumnName("script_id");

                entity.Property(e => e.SshConnectionId).HasColumnName("ssh_connection_id");

                entity.HasOne(d => d.Script)
                    .WithMany(p => p.Router)
                    .HasForeignKey(d => d.ScriptId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("router_script_fk");

                entity.HasOne(d => d.SshConnection)
                    .WithMany(p => p.Router)
                    .HasForeignKey(d => d.SshConnectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("router_ssh_connection_fk");
            });

            modelBuilder.Entity<Script>(entity =>
            {
                entity.ToTable("script", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.command_id_seq'::regclass)");

                entity.Property(e => e.CommandToExecuteId).HasColumnName("command_to_execute_id");

                entity.Property(e => e.ScriptTypeId).HasColumnName("script_type_id");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200);

                entity.Property(e => e.ScriptTypeId).HasColumnName("script_type_id");

                entity.HasOne(d => d.CommandToExecute)
                    .WithMany(p => p.Script)
                    .HasForeignKey(d => d.CommandToExecuteId)
                    .HasConstraintName("script_command_to_exec_fk");

                entity.HasOne(d => d.ScriptType)
                    .WithMany(p => p.Script)
                    .HasForeignKey(d => d.ScriptTypeId)
                    .HasConstraintName("script_script_type_fk")
                    .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<ScriptType>(entity =>
            {
                entity.ToTable("script_type", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.ToTable("settings", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.settings_id_seq'::regclass)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(300);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<SshConnection>(entity =>
            {
                entity.ToTable("ssh_connection", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.ssh_connection_id_seq'::regclass)");

                entity.Property(e => e.HostName)
                    .IsRequired()
                    .HasColumnName("host_name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("character varying");

                entity.Property(e => e.Port).HasColumnName("port");

                entity.Property(e => e.PublicSshKey)
                    .HasColumnName("public_ssh_key")
                    .HasColumnType("character varying");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user", "common");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('common.\"User_id_seq\"'::regclass)");

                entity.Property(e => e.Fio)
                    .HasColumnName("fio")
                    .HasMaxLength(200);

                entity.Property(e => e.Nik)
                    .HasColumnName("nik")
                    .HasMaxLength(200);

                entity.Property(e => e.Password).HasColumnName("password");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(200);

                entity.Property(e => e.IsAdmin).HasColumnName("is_admin");
            });

            modelBuilder.HasSequence<int>("command_id_seq");

            modelBuilder.HasSequence("command_to_execute_id_seq");

            modelBuilder.HasSequence<int>("execute_command_users_id_seq");

            modelBuilder.HasSequence("router_id_seq");

            modelBuilder.HasSequence("settings_id_seq");

            modelBuilder.HasSequence("ssh_connection_id_seq");

            modelBuilder.HasSequence("User_id_seq");
        }
    }
}
