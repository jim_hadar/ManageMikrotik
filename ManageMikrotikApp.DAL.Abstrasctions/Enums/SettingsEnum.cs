﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.DAL.Abstrasctions.Enums {
    public enum SettingsEnum {
        /// <summary>
        /// telegram bot authentification token
        /// </summary>
        AuthentificationToken = 1,
        /// <summary>
        /// telegram bot channel id for send common messages
        /// </summary>
        CommonChannelId,
        /// <summary>
        /// netping username for authentification
        /// </summary>
        NetPinUserName,
        /// <summary>
        /// netping password for authentification
        /// </summary>
        NetPingPassword
    }
}
