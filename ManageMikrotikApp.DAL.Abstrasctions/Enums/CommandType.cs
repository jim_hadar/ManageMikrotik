﻿namespace ManageMikrotikApp.DAL.Abstrasctions.Enums {
    public enum ScriptType {
        ExecuteOnRouter = 1,
        ExecuteOnServer = 2
    }
}
