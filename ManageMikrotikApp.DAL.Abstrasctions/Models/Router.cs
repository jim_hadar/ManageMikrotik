﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class Router : IBaseObject {
        public Router()
        {
            CommandToExecute = new HashSet<CommandToExecute>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? SshConnectionId { get; set; }
        public int? ScriptId { get; set; }

        public Script Script { get; set; }
        public SshConnection SshConnection { get; set; }
        public ICollection<CommandToExecute> CommandToExecute { get; set; }
    }
}