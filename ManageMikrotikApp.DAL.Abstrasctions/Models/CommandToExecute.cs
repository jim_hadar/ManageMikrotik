﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class CommandToExecute : IBaseObject {
        public CommandToExecute()
        {
            Script = new HashSet<Script>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int RouterId { get; set; }

        public Router Router { get; set; }
        public ICollection<Script> Script { get; set; }
    }
}
