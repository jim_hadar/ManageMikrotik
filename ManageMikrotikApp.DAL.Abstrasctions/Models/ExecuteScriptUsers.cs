﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class ExecuteScriptUsers : IBaseObject {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ScriptId { get; set; }
        public bool? IsAllow { get; set; } = true;

        public Script Script { get; set; }
        public User User { get; set; }
    }
}
