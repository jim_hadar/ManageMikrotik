﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class Settings : IBaseObject {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
