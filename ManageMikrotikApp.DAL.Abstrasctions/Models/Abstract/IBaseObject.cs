﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract {
    public interface IBaseObject : IBase {
        int Id { get; set; }
    }
}
