﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class User : IBaseObject
    {
        public User()
        {
            ExecuteScriptUsers = new HashSet<ExecuteScriptUsers>();
        }

        public int Id { get; set; }
        public string Nik { get; set; }
        public string Phone { get; set; }
        public string Fio { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        public ICollection<ExecuteScriptUsers> ExecuteScriptUsers { get; set; }
    }
}
