﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class Script : IBaseObject {
        public Script()
        {
            ExecuteScriptUsers = new HashSet<ExecuteScriptUsers>();
            Router = new HashSet<Router>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CommandToExecuteId { get; set; }
        public int? ScriptTypeId { get; set; }

        public CommandToExecute CommandToExecute { get; set; }
        public ScriptType ScriptType { get; set; }
        public ICollection<ExecuteScriptUsers> ExecuteScriptUsers { get; set; }
        public ICollection<Router> Router { get; set; }
    }
}