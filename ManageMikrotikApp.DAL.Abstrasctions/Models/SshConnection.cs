﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;

namespace ManageMikrotikApp.DAL.Abstrasctions.Models
{
    public partial class SshConnection : IBaseObject {
        public SshConnection()
        {
            Router = new HashSet<Router>();
        }

        public int Id { get; set; }
        public string HostName { get; set; }
        public int Port { get; set; } = 22;
        public string Username { get; set; }
        public string Password { get; set; }
        public string PublicSshKey { get; set; }

        public ICollection<Router> Router { get; set; }
    }
}
