﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;

namespace ManageMikrotikApp.DAL.Abstrasctions.Repository {
    public interface IRepositoryFactory : IDisposable {
        IBaseRepository<T> Create<T>() where T : class, IBaseObject, new();

        IBaseRepository Create();
    }
}
