﻿using ManageMikrotikApp.DAL.Abstrasctions.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ManageMikrotikApp.DAL.Abstrasctions.Repository {
    public interface IBaseRepository<T> : IBaseRepository where T : IBaseObject {
        IQueryable<T> GetAll();

        IQueryable<T> GetAllWithoutChild();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        T Get(int id, bool hidden = false);
        T Update(T entity);
        void Delete(Func<T, bool> predicate);
        void Delete(T entity);
        int Delete(int id);
        T Insert(T entity);
        T Hide(int id);
        void Detach(T entity);
    }

    public interface IBaseRepository : IBase, IDisposable {

    }
}